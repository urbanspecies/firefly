import json, math, re

queue = []

direct_serial = False


def chunkstring(string, length):
    return (string[0 + i:length + i] for i in range(0, len(string), length))


def msg_to_mesh(message):
    re_range = re.compile('(\d+)-(\d+)')

    # Compiled regex for ranges here so we don't do all the hard work in every loop
    # Process message if it actually contains data
    if message and type(message['data']) == bytes:
        try:
            data = json.loads(message['data'].rstrip())
            print(data)

            if not (data.get('data') is None):
                data = data.get('data')
                queue.append(data)
                return

            _type = data.get('type')
            id = data.get('id', 255)
            if type(id) is int:  # did we just send a single id as a number?
                id_list = [id]
            elif type(id) is list:  # or did we send a list of id's?
                id_list = id
            else:  # or did we send a range of id's? ( "1-3")
                id_range = re_range.match(id)
                id_list = range(int(id_range.group(1)), 1 + int(id_range.group(2))) if id_range else [int(id)]  # if not, just a single id as a string

            for id in id_list:
                if _type == 'Node':
                    id_new = data.get('newId', id)  # newId is optional
                    name = data.get('name', "Master" if id_new == 0 else f'Node{id_new}')  # name is optional, name will be Master or Node{id}
                    new_name = name[:20]  # brython can not compile this when it is within "f" string
                    queue.append(f'<N/{id}/{id_new}/{new_name}>')  # limiting name to max 20 characters just in case

                elif _type == 'Radio':
                    datarate = data.get('datarate')
                    powerlevel = data.get('powerlevel')
                    channel = data.get('channel')
                    meshTimeout = data.get('meshTimeout')
                    crcLevel = data.get('crcLevel')
                    queue.append(f'<R/{id}/{datarate}/{powerlevel}/{channel}/{meshTimeout}/{crcLevel}>')

                elif _type == 'Parameters':
                    neoLedCount = data.get('neoLedCount')
                    if type(neoLedCount) is list:
                        neoLedCountA, neoLedCountB = neoLedCount
                    else:
                        neoLedCountA = neoLedCount
                        neoLedCountB = 0
                    neoLedType = data.get('neoLedType', 0)
                    serialBufferLength = data.get('serialBufferLength', 80)
                    serialRedirectMode = data.get('serialRedirectMode', 0)
                    serialRedirectToNode = data.get('serialRedirectToNode', 0)
                    serialRedirectStartMarker = data.get('serialRedirectStartMarker', 10)
                    serialRedirectEndMarker = data.get('serialRedirectEndMarker', 10)
                    serialSpeed = data.get('serialSpeed', 115200)
                    queue.append(
                        f'<P/{id}/{neoLedCountA}/{neoLedType}/{neoLedCountB}/{neoLedType}/{serialBufferLength}/{serialRedirectMode}/{serialRedirectToNode}/{serialRedirectStartMarker}/{serialRedirectEndMarker}/{serialSpeed}>')

                elif _type == 'SetLED':
                    start = data.get('start', 0)
                    length = data.get('length', len(data.get('pixels')))
                    showNow = data.get('showNow', 1)
                    output = data.get('output', 1)
                    _hex = data.get('hex', "")
                    if not (data.get('pixels') is None):
                        pixels = data.get('pixels')
                        length = len(pixels)
                        for pixel in pixels:
                            _hex += '%02x%02x%02x' % tuple(pixel)

                    else:
                        chunkhex = list(chunkstring(_hex, 6 * 8))
                        chunks = len(chunkhex)
                        for chunk in chunkhex:
                            _hex = chunk
                            length = math.floor(len(_hex) / 6)
                            if chunks != 1:
                                queue.append(f'<A/{id}/{start}/{length}/0{output}/{_hex}>')
                                chunks -= 1
                                start += 8
                    queue.append(f'<A/{id}/{start}/{length}/{showNow}/{output}/{_hex}>')

                elif _type == 'FillLED':
                    start = data.get('start', 0)
                    length = data.get('length', 0)
                    showNow = data.get('showNow', 1)
                    output = data.get('output', 1)
                    rgb_r, rgb_g, rgb_b = data.get('rgb')
                    queue.append(f'<F/{id}/{start}/{length}/{showNow}/{output}/{rgb_r}/{rgb_g}/{rgb_b}>')

                elif _type == 'ClearLED':
                    output = data.get('output', 1)
                    queue.append(f'<F/{id}/0/65535/1/{output}/0/0/0>')

                elif _type == 'PlayAnimation':
                    memorybank = data.get('memorybank')
                    speed = data.get('speed')
                    flow = data.get('flow', 0)
                    repeat = data.get('repeat', 0)
                    output = data.get('output', 1)
                    queue.append(f'<B/{id}/{memorybank}/{speed}/{flow}/{repeat}/{output}>')

                elif _type == 'StopAnimation':
                    queue.append(f'<B/{id}/255/0/0/0/0>')

                elif _type == 'ClearAnimation':
                    queue.append(f'<B/{id}/254/0/0/0/0>')

                elif _type == 'SaveAnimation':
                    memorybank = data.get('memorybank')
                    start = data.get('start', 0)
                    length = data.get('length', 0)
                    showNow = data.get('showNow', 1)
                    output = data.get('output', 1)
                    _hex = data.get('hex', "")
                    if not (data.get('pixels') is None):
                        pixels = data.get('pixels')
                        chunks = math.ceil(len(pixels) / 8)  # amount of packages to send of max length 8 (8=1,9=2,16=2, 18=3,...)
                        for pixel in pixels:
                            _hex += '%02x%02x%02x' % tuple(pixel)
                            length += 1
                            if length == 8 and chunks != 1:
                                queue.append(f'<M/{id}/{memorybank}/{length}/{start}/{_hex}>')
                                _hex = ""  # Reset everything for the new packet
                                length = 0
                                chunks -= 1
                                start += 8
                    else:
                        chunkhex = list(chunkstring(_hex, 6 * 8))
                        chunks = len(chunkhex)
                        for chunk in chunkhex:
                            _hex = chunk
                            length = math.floor(len(_hex) / 6)
                            if chunks != 1:
                                queue.append(f'<M/{id}/{memorybank}/{length}/{start}/{_hex}>')
                                chunks -= 1
                                start += 8
                    queue.append(f'<M/{id}/{memorybank}/{length}/{start}/{_hex}>')

                elif _type == 'ConfigureAnimation':
                    memorybank = data.get('memorybank')
                    autoplay = data.get('autoplay', 1)
                    ledsPerFrame = data.get('ledsPerFrame')
                    frameCount = data.get('frameCount')
                    speed = data.get('speed')
                    flow = data.get('flow', 0)
                    repeat = data.get('repeat', 255)
                    output = data.get('output', 1)
                    queue.append(f'<L/{id}/{memorybank}/{autoplay}/{ledsPerFrame}/{frameCount}/{speed}/{flow}/{repeat}/{output}>')

                elif _type == 'DMX':
                    channel = data.get('channel')
                    value = data.get('value')
                    queue.append(f'<D/{id}/{channel}/{value}>')

                elif _type == 'MOSFET':
                    output = data.get('output')
                    state = data.get('state')
                    if type(state) is not int:
                        state = 1 if state == "HIGH" else 0
                    queue.append(f'<O/{id}/{output}/{state}>')

                else:
                    print("Unknown type!")
                    # logging.debug("Unknown type!")

        except (ValueError, KeyError) as e:
            print("Not Valid JSON")
            # logging.info("Not valid json")
        return queue


if not direct_serial:
    import sys, logging, time, signal, serial, redis, json, os
    from datetime import datetime

    serial_port = sys.argv[1]

    # Propper logging is important
    logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', stream=sys.stdout, level=logging.DEBUG)

    # Connection to Redis server
    r = redis.from_url(os.getenv("REDIS_URL", "redis://localhost:6379"))
    p = r.pubsub(ignore_subscribe_messages=True)

    p.psubscribe(**{'toMesh': msg_to_mesh})
    p.run_in_thread(sleep_time=.01)  # 0.01 is the most optimised sleep value for linux


    # Following block handles closing of the script
    def sigterm_handler(_signo, _stack_frame):
        # Raises SystemExit(0):
        sys.exit(0)
        # os._exit()


    signal.signal(signal.SIGINT, sigterm_handler)

    try:
        logging.info(f'Device connected on /dev/{serial_port}')
        with serial.Serial(f'/dev/{serial_port}', 115200, timeout=0) as serial_connection:
            buffer = ''
            while (True):
                try:
                    bytes_ready = serial_connection.inWaiting()
                    if bytes_ready > 0:
                        for _ in range(bytes_ready):
                            last_byte = serial_connection.read(1).decode('ascii')
                            buffer += last_byte
                            if last_byte == '\n':
                                data = buffer.rstrip()
                                buffer = ''
                                try:
                                    json.loads(data)
                                except ValueError as e:
                                    _data = {}
                                    _data["debug"] = data
                                    data = json.dumps(_data)
                                r.publish(f'fromMesh', data)
                    if queue:
                        message = queue.pop(0)
                        # if type(message) is tuple:
                        #    priority = message[0]
                        #    message = message[1]
                        try:
                            serial_connection.write(message.encode())
                            logging.debug(f'Sending "{message}"')
                            serial_connection.flush()
                        except AttributeError as e:
                            pass
                        time.sleep(0.1)
                except (IOError, TypeError) as e:
                    # serial_connection.close()
                    pass
                time.sleep(0.001)
    finally:
        logging.info(f'Device disconnected from /dev/{serial_port}')
        # os.kill(os.getpid(),signal.SIGKILL)
