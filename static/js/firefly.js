/*
Array.prototype.remove = function() {

    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
*/

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


//check if DOM is loaded before doing addEventListeners in following code.
window.addEventListener("DOMContentLoaded", function () {
//the whole codeblock is in this eventlistener event which gets execute when the whole DOM is loaded.


// init brython and read settings file
    window.onload = init()


    let currentFrame = 0,								//point to the framenumber the user is currently working on.
        currentLayer = 2,                               // point to the active layer
        currentSpeed = 250,								//default playback speed
        bounce = false,							        //play state, should i bounce or repeat on looping
        bounceUp = true,
        repeat = false,							        //play state, should i bounce or repeat on looping
        playing = false,							    //is the animation playing in realtime
        frameList,
        frameLayerList,
        frameLayerNames,
        frameEnabledLayers,
        numberOfLayers = 3,
        animation,
        animatedCurrentFrame = 0,
        currentNodeID = null, // unused?
        nodeList = document.getElementById("tb_lightboxSelect"),
        direction = 0, // rotation direction
        port = null, // serial port
        sendBlinker = true;


    function getLightboxID() {
        return parseInt(nodeList.value);
    }

    currentSaveFile = "";							//holds the filename of the savefile when
    selectedDrawTool = document.getElementById("btn_pencil");	//set the selected tool to pencil at start.
    viewmasterStartfrom = 0;							//the frame preview buttons 1-9 at the bottom of the led view.
    viewmasterPointer = 0;							//holds currently selected viewpoint
    writeto = false;
    colorPointer = 3; 							//counter used to cycle through colors wih keyboard.
    //starts at 3 because colorbox3 is the startcolor (red).
    /* OK */
    function randomId() {								//function used to generate a unique cookie-id.
        const uint32 = window.crypto.getRandomValues(new Uint32Array(1))[0];
        return uint32.toString(16);
    }

    /* OK */
    function getCookie(name) {							//return the value of a stored cookie.
        let dc = document.cookie,
            prefix = name + "=",
            begin = dc.indexOf("; " + prefix);
        if (begin == -1) {
            begin = dc.indexOf(prefix);
            if (begin != 0) return null;
        } else {
            begin += 2;
            var end = document.cookie.indexOf(";", begin);
            if (end == -1) {
                end = dc.length;
            }
        }
        return decodeURI(dc.substring(begin + prefix.length, end));
    }

    let authToken = randomId();
    //document.cookie = 'userId=' + authToken + '; path=/'; //mind the space after ; !
    myCookie = getCookie("userId");
    console.log(document.cookie);
    /* OK */
    if (myCookie == null) {											//if cookie does not exist -> we have a new user
        document.cookie = 'userId=' + authToken + '; path=/';		//create cookie with these values.. mind the space after; !
        document.cookie = 'defaultRows=6' + '; path=/';				//doesnt work on all platforms if space is missing
        document.cookie = 'defaultCols=6' + '; path=/';
        document.cookie = 'defaultLeds=54' + '; path=/';
        document.cookie = 'userName=Anonymous' + '; path=/';
        document.cookie = 'path=/' + '; path=/';

        console.log("Created new cookie, userId=" + authToken);
    } else {
        console.log("Cookie found, userId=" + myCookie);			//user already visited and has a cookie
    }

    ledRows = 6;
    ledCols = 9;

    //max leds at start, this can be changed from the GUI be changing
    MAX_LEDS = 54;

    function addNodeID(nodeID) {
        console.log("node found: " + nodeID);
        for (var i = 0; i < nodeList.length; i++) {
            var option = nodeList.options[i];
            if (option.value == -1) {
                option.text = "Pick a node";
            } else if (option.value == nodeID) {
                return;
            }
        }
        nodeList.add(new Option("Node " + nodeID, nodeID));
        setStatusText("Node " + nodeID + " online", "normal");

    }

    function removeNodeID(nodeID) {
        console.log("node disconnected: " + nodeID);

    }



    var mouseDown = false;											//variabble holding current mouseState
    document.body.onmousedown = function () {
        mouseDown = true;
    }	//save mouseDown status on mouseUp
    document.body.onmouseup = function () {
        mouseDown = false;
    }	//save mouseDown status on mouseDown

    /* REMOVE?
    websocket.onmessage = function (event) {						//process incoming websockets messages here
        console.log("Incoming data :" + event.data);
        //example of JSON msg : {"msgtype": "getLightboxList", "dataWritingPipesPool": [2, 3, 4, 5, 6], "dataWritingPipes": [1, 2, 3, 4, 5, 6], "dataSelfPipe": 1}

        var msg = JSON.parse(event.data);
        if(msg.hasOwnProperty("msgtype"))
        {
            if (msg.msgtype=='getFileList'){								//build the loadFile dropdown list from the json data
                buildFileList(msg.data);
            }

            if (msg.msgtype=='getLightboxList'){
                console.log(msg.data)										//build the selectNode dropdown list from the json data
                buildLightboxList(msg.dataWritingPipesPool, msg.dataWritingPipes, msg.dataSelfPipe);
            }

            if (msg.msgtype=='loadFile'){									//what to do when the lbController sends the data of the file we want to load.
                console.log(msg.data);
                frameList = msg.data;										//copy whole animation into frameList variable
                currentFrame = 0;											//set currentFrame pointer to 0, first frame
                console.log(Object.keys(frameList[0]).length);
                MAX_LEDS = Object.keys(frameList[0]).length;				//get dictionary size of the first frame. From this value, you know how many leds there are in the frame.
                dimensionBtn( msg.rows, msg.cols, Object.keys(frameList[0]).length, msg.doLedDirection, false);	//create a new ledDirection array with the new height/rows
                console.log(msg.doLedDirection)
                document.getElementById("ledDirection").checked= msg.doLedDirection;		//if ledDirection as asked, mark is so in the userSettings window.
                //sendMod('gotoFrame', currentFrame)
                viewmasterStartfrom = 0;									//reset viewmaster to first frame
                viewmasterPointer = 0;
                renderViewMaster();											//show correct viewmaster update
                renderLeds();
            }
        }
    }
    */
    //Build animation matrix of leddata
    //Eaxh animation has multiple frames so we stored all frame data in an array (a list becuse the position in the array is important)
    //Every frameList item points to a dictionary with all leddata of the frame. This is a dictionary.
    //dictionary key value always get converted to string with json.

    frameList = [];  //create array
    frameList[0] = {}; //create first frame as dictionary

    for (let i = 0; i < MAX_LEDS; i++) {
        frameList[0][i] = "0:0:0"			//fill first frame dictionary with empty RGB values.
    }

    frameEnabledLayers = {0: true, 1: true, 2: true};
    frameLayerNames = {0: 'Layer-0', 1: 'Layer-1', 2: 'Layer-2'};

    frameLayerList = [];  // create array
    frameLayerList[0] = []


    // create layers and empty frames
    for (let i = 0; i < numberOfLayers; i++) {
        frameLayerList[0][i] = {}
        for (let j = 0; j < MAX_LEDS; j++) {
            frameLayerList[0][i][j] = "0:0:0"
        }
    }

    let copyPaste = {},  //make empty dict frame to use as  a copy/paste buffer
        copyPasteFrameLayer;
    renderViewMaster();
    document.getElementById("tb_colorselect").style.backgroundColor = "red"

    /////////////////////////////////////
    // Start of EVENT LISTENERS adding
    /////////////////////////////////////
    //activate changeCurrentColor on each element of the colorbox class.
    let colorbox = document.getElementsByClassName("colorbox");
    for (var i = 0; i < colorbox.length; i++) colorbox[i].addEventListener("click", changeCurrentColor);

    //activate changeCurrentColor on each element of the colorbox class.
    function attachLedActions() {
        //For every LED box, we add two functions :
        //		onClick -> this function sends changed single LED color data to the lbController.
        //		onHovr  -> this function sends mouse cursor position to the lbController.
        let leds = document.getElementsByClassName("boxed_led");
        for (let i = 0; i < leds.length; i++) leds[i].addEventListener("click", changeLedColor);
        for (let i = 0; i < leds.length; i++) leds[i].addEventListener("mouseover", changeLedColorHover);

        //For every frame goto button, we add two functions :
        //		onClick -> Function asks the lbController to show other frame.
        //		onHover  -> Function copies duplicates the previous frame for easy use.
        let previews = document.getElementsByClassName("boxed_preview");
        for (let i = 0; i < previews.length; i++) previews[i].addEventListener("click", previewClick);
        for (let i = 0; i < previews.length; i++) previews[i].addEventListener("dblclick", previewDubbelClick);
    }

    attachLedActions();		//runs previous function

    // document.getElementById("speedSliderMembank").addEventListener("input", speedSliderMembankChange);	//Change playback speed of a animation to save.
    document.getElementById("speedSlider").addEventListener("input", speedSliderChange);		//Change placyback speed of live play
    document.getElementById("speedSlider").addEventListener("mouseup", speedSliderMouseUp);	//for desktop support
    document.getElementById("speedSlider").addEventListener("touchend", speedSliderMouseUp);	//for touch based support
    document.getElementById("btn_pencil").addEventListener("click", selectDrawTool);		//all 3 draw btns points point to same function
    document.getElementById("btn_filler").addEventListener("click", selectDrawTool);		//
    document.getElementById("btn_eyedropper").addEventListener("click", selectDrawTool);		//
    document.getElementById("btn_clearall").addEventListener("click", clearAll);				//clear all pixels function
    document.getElementById("tb_colorselect").addEventListener("click", colorSelect);			//change color function
    document.getElementById("btn_insertframe").addEventListener("click", insertFrame);
    document.getElementById("btn_deleteframe").addEventListener("click", deleteFrame);
    document.getElementById("btn_previous").addEventListener("click", previous);
    document.getElementById("btn_next").addEventListener("click", next);
    document.getElementById("btn_firstframe").addEventListener("click", firstFrame);
    document.getElementById("btn_lastframe").addEventListener("click", lastFrame);
    document.getElementById("btn_copy").addEventListener("click", copy);					//copy whole frame to buffer function
    document.getElementById("btn_paste").addEventListener("click", paste);					//paste whole frame to buffer function
    document.getElementById("btn_play").addEventListener("click", play);
    document.getElementById("btn_stop").addEventListener("click", stop);
    document.getElementById("btn_repeat").addEventListener("click", repeatBtn);				//'repeat' is a reserved word. changed name to repeatBtn instead.
    document.getElementById("btn_bounce").addEventListener("click", bounceBtn);
    document.getElementById("btn_save").addEventListener("click", saveOrUploadModal);
    document.getElementById("btn_save_animation").addEventListener("click", saveFile);
    document.getElementById("btn_upload_animation").addEventListener("click", submitUploadForm);
    // document.getElementById("tb_fileselect").addEventListener("change", loadFile);				//if user clicks a file to load
    document.getElementById("tb_lightboxSelect").addEventListener("change", lightboxSelect);		//user selects a new lightbox from dropdown
    document.getElementById("tb_lightboxSelect").addEventListener("click", chooseSerialDevice);		//user selects a new lightbox from dropdown
    document.getElementById("applyNewSettingsBtn").addEventListener("click", applyChanges);			//user changes row/col dimension in userSettings menu
    document.getElementById("saveDefaultsBtn").addEventListener("click", saveDefaults);			//user saves default userSettings parameters and save it in cookie
    // document.getElementById("memSaveBtn").addEventListener("click", MemorySave);		//Exec MemorySave() when clicked on MemorySave button
    document.getElementById("saveOrSendCommandBtn").addEventListener("click", saveOrSendCommand);		//Send Command
    document.getElementById("defaultRows").addEventListener("input", changeRowsOrColumns);
    document.getElementById("defaultCols").addEventListener("input", changeRowsOrColumns);
    document.getElementById("chooseFileDivId").addEventListener("click", uploadFile);
    document.getElementById("defaultCols").addEventListener("input", changeRowsOrColumns);
    document.getElementById("chooseFileDivId").addEventListener("click", uploadFile);
    document.getElementById("editLayerNameButtonId").addEventListener("click", saveLayerName);
    document.getElementById("move_down_layer_id").addEventListener("click", layerMoveDownUp);
    document.getElementById("move_up_layer_id").addEventListener("click", layerMoveDownUp);
    document.getElementById("duplicate_layer_id").addEventListener("click", duplicateLayer);

    document.getElementById("btn_pencil").click();			//set pencil as initial selected tool.
    repeatBtn(); 											//set repeat button as initial state of repeat button.

    // add listener for rotations
    /*let radios = document.getElementsByName('rotate');
    for (let i = 0; i < radios.length; i++) {
        radios[i].addEventListener('change', function () {
            direction = parseInt(this.value);
            dimensionBtn(ledRows, ledCols, MAX_LEDS, false, true);
        });
    }*/

    //Show the popup message with keyboard shortcuts you can use.
    document.getElementById("btn_help").addEventListener("click", function () {
        helptext = "T = select tool\nP = play\nS = stop\nC = copy frame\nD = Download animation\nV = paste frame\nE = eraser all\nB = bounce\nR = repeat\nleft = previous frame\nright = next frame\nUp = Colorcyle up\nDown = Colorcyle down\ninsert = insert frame\ndel = delete frame\nhome/pageup = first frame\nend/pagedown = last frame\n";
        alert(helptext);
    });

    //variables pointing to various elements for easy access.
    let btn_memsave = document.getElementById("btn_memsave"),
        btn_settings = document.getElementById("btn_settings"),
        membankModal = document.getElementById("membankModal"),
        settingsModal = document.getElementById("settingsModal"),
        saveOrUploadWindow = document.getElementById("saveOrLoadModal"),
        editLayerNameModal = document.getElementById("editLayerNameModal"),
        membankCloseBtn = document.getElementById("membankCloseBtn"), 		// Get the <span> element that closes the modal window
        settingsCloseBtn = document.getElementById("settingsCloseBtn"), 	// Get the <span> element that closes the modal window
        saveOrUploadCloseBtn = document.getElementById("saveOrLoadCloseBtn"),
        editLayerNameCloseBtn = document.getElementById("editLayerNameCloseBtn");

    /* OK */
    // When the user clicks the save to memory-bank-button, open the modal window.
    btn_memsave.onclick = async function () {
        document.getElementById("node").value = getLightboxID();
        membankModal.style.display = "block";

        document.getElementById("commandSpeed").value = currentSpeed;
        document.getElementById("commandSpeedSpan").value = currentSpeed;
        document.getElementById("commandSpeedSpan").innerHTML = currentSpeed;
    }

    /* OK */
    btn_settings.onclick = function () {										//Function which sets default values in userSetting menu
        document.getElementById("settingsErrorLabel").innerHTML = "";

        // newName = document.getElementById("defaultUsername");
        let newRows = document.getElementById("defaultRows"),
            newCols = document.getElementById("defaultCols"),
            newLeds = document.getElementById("defaultLeds");

        // commented those because we are running index.html without server and cookies does not work
        // newName.value = getCookie("userName");
        // newRows.value = getCookie("defaultRows");
        // newCols.value = getCookie("defaultCols");
        // newLeds.value = getCookie("defaultLeds");
        newRows.value = ledRows;
        newCols.value = ledCols;
        newLeds.value = MAX_LEDS;

        settingsModal.style.display = "block";								//show window as modal

        let rotateModals = document.getElementsByName('rotate');
        rotateModals[direction].checked = true;
    }

    membankCloseBtn.onclick = function () {									// Close window when clicking on the X of the membank save window
        membankModal.style.display = "none";
    }

    settingsCloseBtn.onclick = function () {									// Close window when clicking on the X of the membank save window
        settingsModal.style.display = "none";
    }


    saveOrUploadCloseBtn.onclick = function () {
        saveOrUploadWindow.style.display = "none";
    }

    editLayerNameCloseBtn.onclick = function () {
        editLayerNameModal.style.display = "none";
    }

    // When the user clicks anywhere outside of the membank save window of user settings window, close it.
    window.onclick = function (event) {
        if (event.target === membankModal || event.target === settingsModal ||
            event.target === saveOrUploadWindow || event.target === editLayerNameModal) {
            membankModal.style.display = "none";
            settingsModal.style.display = "none";
            saveOrUploadWindow.style.display = "none";
            editLayerNameModal.style.display = "none";
        }
    }

    /* OK */
    function addCss(rule) {
        let css = document.createElement('style');
        css.type = 'text/css';
        if (css.styleSheet) css.styleSheet.cssText = rule;					// Support for IE
        else css.appendChild(document.createTextNode(rule));				// Support for the rest
        document.getElementsByTagName("head")[0].appendChild(css);			//add css rule to the HEAD of the document
    }

    /* OK */
    function setStatusText(message, priority) {								//shows a sliding popup from the top with custom message
        console.log("Popup:" + message);
        note = document.getElementById("note");
        if (priority == "normal") {
            note.style.backgroundColor = "fde073";
        }		//use different color depending on msg type
        if (priority == "urgent") {
            note.style.backgroundColor = "ff9933";
        }
        note.innerHTML = message;
        note.classList.remove("noteanimation");								//first remove animation
        note.offsetWidth; 													//https://css-tricks.com/restart-css-animation/
        note.classList.add("noteanimation");								//then add animation to activate it
    }

    /* OK */
    function selectDrawTool(event) {											//Visualise the selected tool by inverting its background color.
        selectedDrawTool.style.filter = 'none'								//reset style of current selectedDrawTool
        this.style.backgroundColor = "white";
        this.style.filter = "invert(100%)";
        selectedDrawTool = this;											//remember which tool was selected to use in other functions
    }


    /* NOK: dependancy sendFrame */
    function clearAll() {													//change all LEDS to black and send frame update to lbController.
        let leds = document.getElementsByClassName("boxed_led");
        for (var i = 0; i < leds.length; i++) {
            leds[i].classList.add("ledview_bg");
            leds[i].style.backgroundColor = "#fff";
            frameList[currentFrame][i] = "0:0:0";
            frameLayerList[currentFrame][currentLayer][i] = "0:0:0";
        }
        sendFrame(frameList)
    }

    /* OK */

    //Function to change the color to use.
    function changeCurrentColor(event) {
        if (event.target.id === "tb_color_11") {								//In case of drawing 'nothing' aka transparant.
            document.getElementById("tb_colorselect").classList.add("transparantBg");
            //remove the elements which are added by the color pick plugin at start.
            document.getElementById("tb_colorselect").style.removeProperty("background-image");
            document.getElementById("tb_colorselect").style.removeProperty("background-color");
            document.getElementById("tb_colorselect").style.removeProperty("color");
        } else {
            document.getElementById("tb_colorselect").classList.remove("transparantBg");
            document.getElementById("tb_colorselect").style.backgroundColor = getComputedStyle(document.getElementById(this.id)).backgroundColor;
        }
    }

    /* OK */

//If user clicks the big selected-color box, change current drawing tool to pen.
    function colorSelect() {
        document.getElementById("btn_pencil").click();
    }

    /* UPDATE */

    //function used to send which LED field the mouse if hoverring over. A blinking cursor is displayed on the lightbox.
    function changeLedColorHover(event) {
        // if we are picking color do nothing
        if (pickingColor) return;


        if (mouseDown === true) {
            this.click(); 									//makes you 'click and slide' to keep drawing
            return;											//return so it does not do a blinker to so overload.
        }

        if (getLightboxID() === -1 || !sendBlinker) return; 									//dont send blinker data when no lightbox is selected.


        let selectedLed = parseInt(this.dataset.nr) - 1;							//leds numbering start at 1 html layout, but 0 in Lbcontroller.

        // UPDATE: to new post
        toMesh({"type": "PlayAnimation", "id": getLightboxID(), "memorybank": 253, "speed": selectedLed});

    }

    //Function
    function changeLedColor(event) {
        // if we are picking color do nothing
        if (pickingColor) return;


        selectedLed = parseInt(this.dataset.nr) - 1;

        //when using pencil tool..
        if (selectedDrawTool === document.getElementById("btn_pencil")) {							//in case pencil + transparant color is selected
            if (document.getElementById("tb_colorselect").classList.contains("transparantBg")) {
                this.style.backgroundColor = "#fff";											//add white background
                this.classList.add("ledview_bg");												//add the stripes on top of the white background
                frameList[currentFrame][selectedLed] = "0:0:0";									//set to black (=transparant)
                frameLayerList[currentFrame][currentLayer][selectedLed] = "0:0:0";
            } else {																				//in case pencil + a color is selected
                this.style.backgroundColor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor;
                this.classList.remove("ledview_bg");											//remove the stripes background
                let rgbcolor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor,
                    rgb = rgbcolor.match(/\d+/g);	//extract color information with regex from previous getComputedStyle string
                red = rgb[0];
                green = rgb[1];
                blue = rgb[2];
                frameList[currentFrame][selectedLed] = rgb[0] + ":" + rgb[1] + ":" + rgb[2];	//update internal LED data with color update
                frameLayerList[currentFrame][currentLayer][selectedLed] = rgb[0] + ":" + rgb[1] + ":" + rgb[2];
            }
            sendSingleLedJSON(selectedLed, frameList[currentFrame][selectedLed]);				//send led color update

        }

        //when using filler tool..
        if (selectedDrawTool === document.getElementById("btn_filler")) {							//select color fill tool

            if (document.getElementById("tb_colorselect").classList.contains("transparantBg")) {	//if transparant color selected ?
                let allLeds = document.getElementsByClassName("boxed_led");
                for (let i = 0; i < allLeds.length; i++) {
                    allLeds[i].style.backgroundColor = "#fff";									//set background to white
                    allLeds[i].classList.add("ledview_bg");										//add the stripes that white background
                    frameList[currentFrame][i] = "0:0:0";
                    frameLayerList[currentFrame][currentLayer][i] = "0:0:0";
                }
            } else {																				//if NON transparant color selected ?
                let allLeds = document.getElementsByClassName("boxed_led"),
                    rgbcolor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor,
                    rgb = rgbcolor.match(/\d+/g);
                red = rgb[0];
                green = rgb[1];
                blue = rgb[2];

                for (let i = 0; i < allLeds.length; i++) {
                    allLeds[i].style.backgroundColor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor;
                    allLeds[i].classList.remove("ledview_bg");									//remove the stripes background

                    frameList[currentFrame][i] = rgb[0] + ":" + rgb[1] + ":" + rgb[2];
                    frameLayerList[currentFrame][currentLayer][i] = rgb[0] + ":" + rgb[1] + ":" + rgb[2];
                }
            }
            sendFrame(frameList);																//Send ALL leds because they have all changed.
        }

        //when using eyedropper tool
        if (selectedDrawTool === document.getElementById("btn_eyedropper")) {
            document.getElementById("tb_colorselect").style.backgroundColor = getComputedStyle(document.getElementById(this.id)).backgroundColor;
            document.getElementById("btn_pencil").click();
        }

        updateFrameList();

    }

    //Function to redraw all led fields with its color value.
    function renderLeds() {
        document.getElementById("tb_currentframe_text").innerHTML = (currentFrame + 1) + "/" + frameList.length;	//update gui element
        for (let i = 0; i < MAX_LEDS; i++) {																		//loop over all leds of current frame
            let ledtodo = "ledview_" + (i + 1).toString(),
                rgb = frameList[currentFrame][i],
                rgbSplit = rgb.split(":"),
                rgbString = "rgb(" + rgbSplit[0] + "," + rgbSplit[1] + "," + rgbSplit[2] + ")";				//extract rgb data from each led

            if (rgbSplit[0] === "0" && rgbSplit[1] === "0" && rgbSplit[2] === "0") {								//0:0:0 means all black
                document.getElementById(ledtodo).classList.add("ledview_bg");							//add the stripes background
                document.getElementById(ledtodo).style.backgroundColor = "white";
            } else {
                document.getElementById(ledtodo).style.backgroundColor = rgbString;							// it has a color other than full black
                document.getElementById(ledtodo).classList.remove("ledview_bg");							//remove the stripes background
            }
        }
    }


    //This function is to calculate the number of leds
    function changeRowsOrColumns() {
        let rows = document.getElementById('defaultRows').value,
            cols = document.getElementById('defaultCols').value,
            maxLed = document.getElementById('defaultLeds');

        rows = rows !== "" ? parseInt(rows) : 0;
        cols = cols !== "" ? parseInt(cols) : 0;
        maxLed.value = rows * cols;
    }

    //Add new frame in frameList and assign new dictionary entry to it. Fill it with led values after.
    function insertFrame() {
        frameList.splice(currentFrame + 1, 0, frameList[currentFrame + 1]);
        frameList[currentFrame + 1] = {};
        for (let i = 0; i < MAX_LEDS; i++) { //add led with rgb value for each
            frameList[currentFrame + 1][i] = "0:0:0"
        }

        frameLayerList.splice(currentFrame + 1, 0, frameLayerList[currentFrame + 1]);
        frameLayerList[currentFrame + 1] = [];

        for (let i = 0; i < numberOfLayers; i++) {
            frameLayerList[currentFrame + 1][i] = {}
            for (let j = 0; j < MAX_LEDS; j++) { //add led with rgb value for each
                frameLayerList[currentFrame + 1][i][j] = "0:0:0"
            }
        }
        currentFrame += 1;
        sendFrame(frameList, currentFrame);
        updateViewmaster('up')
        renderViewMaster();
        renderLeds();
    }

    //Deleted a frame and redraws the leds and viewmaster buttons.
    function deleteFrame() {
        if (frameList.length > 1) {
            if (currentFrame === (frameList.length) - 1) {
                console.log("last frame detected")
                frameList.splice(currentFrame, 1);
                frameLayerList.splice(currentFrame, 1);
                currentFrame = frameList.length - 1;

                if (frameList.length >= 9) viewmasterStartfrom--;
                else updateViewmaster('down');

            } else if (currentFrame === 0) {
                console.log("first frame detected")
                frameList.splice(currentFrame, 1);
                frameLayerList.splice(currentFrame, 1);
                currentFrame = 0;
            } else {
                frameList.splice(currentFrame, 1);
                frameLayerList.splice(currentFrame, 1);
                if (currentFrame + (9 - viewmasterPointer) > frameList.length && frameList.length >= 9) {
                    viewmasterStartfrom--;
                    currentFrame--;
                }
            }
            updateFrameList();
        }   
    }

    //function keeps tracks where the highlighted previewbox is pointing to
    //so its scrolls correctly when using previous and next buttons.
    function updateViewmaster(updown) {

        if (updown === 'up') {
            if (viewmasterPointer === ledCols - 1) viewmasterStartfrom++;
            if (viewmasterPointer < ledCols - 1) viewmasterPointer++;
        }
        if (updown === 'down') {
            if (viewmasterPointer === 0) viewmasterStartfrom--;
            if (viewmasterPointer > 0) viewmasterPointer--;
        }

        if (viewmasterStartfrom < 0) viewmasterStartfrom = 0;
        console.log(updown + " Pointer:" + viewmasterPointer.toString() + " Startfrom:" + viewmasterStartfrom.toString())
        renderViewMaster()
    }

    //Function to draw the preview buttons at the bottom.
    function renderViewMaster() {
        let cols;
        if (direction % 4 === 0 || direction % 4 === 2) cols = ledCols;
        else cols = ledRows;


        console.log("Rendering preview boxes.")
        document.getElementById("tb_currentframe_text").innerHTML = (currentFrame + 1) + "/" + frameList.length;
        for (i = 0; i < cols; i++) { //clear all preview boxes
            preview = "preview_" + (i + 1).toString();
            document.getElementById(preview).innerHTML = "";
            document.getElementById(preview).style.backgroundColor = "white";
            document.getElementById(preview).style.cursor = "pointer";
        }

        let frameCount = frameList.length;
        if (frameCount > cols) frameCount = cols;

        for (i = 0; i < cols; i++) {
            preview = "preview_" + (i + 1).toString();
            document.getElementById(preview).style.cursor = "pointer";
            if (i < frameCount) {
                document.getElementById(preview).innerHTML = viewmasterStartfrom + i + 1;
            } else {
                document.getElementById(preview).innerHTML = "&nbsp;";
                document.getElementById(preview).style.cursor = "default";
            }
            document.getElementById(preview).style.backgroundColor = "black";
            document.getElementById(preview).style.color = "white"

            if (i === viewmasterPointer) {
                document.getElementById(preview).style.backgroundColor = "black";
                document.getElementById(preview).style.color = "red";
            }
        }
    }

    //jump to next frame and adjust gui
    function next() {
        if (currentFrame < frameList.length - 1) {
            currentFrame += 1;
            renderLeds();
            sendFrame(frameList, currentFrame);
            updateViewmaster('up');
            updateFrameList();
        }
    }

    //jump to previous and adjust gui
    function previous() {
        console.log(currentFrame);
        if (currentFrame > 0) {
            currentFrame -= 1;
            renderLeds();
            sendFrame(frameList, currentFrame);
            updateViewmaster('down');
            updateFrameList();
        }
    }

    //jump to firtframe and adjust gui
    function firstFrame() {
        currentFrame = 0;
        viewmasterPointer = 0;
        viewmasterStartfrom = 0;
        updateFrameList();
    }

    //jump to lastframe and adjust gui
    function lastFrame() {
        currentFrame = frameList.length - 1;
        if (frameList.length > 9) {
            viewmasterPointer = 8;
            viewmasterStartfrom = frameList.length - 9;
        } else {
            viewmasterPointer = frameList.length - 1;
            viewmasterStartfrom = 0;
        }
        updateFrameList();
    }

    //If you click a preview box, you jump to the right frame
    function previewClick() {
        clickedNr = parseInt(this.dataset.nr);
        if (clickedNr > frameList.length - 1) return;			//skip if clicking on empty preview div.
        currentFrame = (viewmasterStartfrom + clickedNr);

        viewmasterPointer = clickedNr;
        updateFrameList();
    }

    //the last frame number of the preview buttons is clickable.
    //if you click it, you will duplicate current frame to the newly next one.
    function previewDubbelClick() {
        console.log("nen dubbel");
        clickedNr = parseInt(this.dataset.nr);

        if (clickedNr + viewmasterStartfrom === frameList.length - 1) { 	//exec this function only on the first empty preview button.
            console.log("execute double");
            currentFrame = frameList.length - 1; 						//set currentframe to last frame.
            let quickCopyFrames = Object.assign({}, frameList[currentFrame]), 	//copy contents of last frame
                quickCopyFrameLayers = frameLayerList[currentFrame].slice();
            insertFrame();											//insert empty frame afger last frame
            frameList[currentFrame] = Object.assign({}, quickCopyFrames); //currentFrame is now to +1, restore contents from copy
            frameLayerList[currentFrame] = quickCopyFrameLayers
            updateFrameList();
        }
    }

    //you can copy a whole frame
    function copy() {
        copyPasteFrameLayer = frameLayerList[currentFrame];
    }

    //and paste it on another frame
    function paste() {
        frameLayerList[currentFrame] = JSON.parse(JSON.stringify(copyPasteFrameLayer));
        updateFrameList();
    }

    //stop the animation played by current user
    function stop() {
        console.log("stopping animation");

        playing = false;
        playBtn = document.getElementById("btn_play");
        playBtn.style.filter = "invert(0%)";
        playBtn.style.backgroundColor = "white";

        clearInterval(animation);
    }

    function player() {
        // if (getLightboxID() === -1) return;
        if (animatedCurrentFrame === frameList.length) {
            if (repeat === false && bounce === false) {
                clearInterval(animation);
                return;
            }
            if (bounce) {
                animatedCurrentFrame -= 2;
                bounceUp = false;
            } else {
                animatedCurrentFrame = 0;
                bounceUp = true;
            }
        } else if (animatedCurrentFrame === -1) {
            animatedCurrentFrame = 1;
            bounceUp = true;
        }

        viewmasterPointer = animatedCurrentFrame;
        currentFrame = animatedCurrentFrame;
        updateFrameList(false);

        bounceUp ? animatedCurrentFrame++ : animatedCurrentFrame--;

        let flow = 'once';
        if (repeat) flow = 'repeat';
        if (bounce) flow = 'bounce';
        console.log({action: 'play', lightboxNr: getLightboxID(), flow: flow, speed: currentSpeed});
    }

    //Start animation play and visualise it by toggling play button
    function play() {
        animatedCurrentFrame = 0;
        console.log("playing animation");
        playing = !playing;									//variable keeps state if you click it or not
        let playBtn = document.getElementById("btn_play");
        if (playing) {
            playBtn.style.filter = "invert(100%)";
            playBtn.style.backgroundColor = "white";
        } else {
            playBtn.style.filter = "invert(0%)";
            playBtn.style.backgroundColor = "white";
            stop();
            return;
        }
        // set current frame to 0
        currentFrame = 0

        animation = setInterval(player, currentSpeed);


    }

    //function enables bounce mode and visualise it by inverting colors.
    function bounceBtn() {
        bounce = !bounce;
        btn_bounce = document.getElementById("btn_bounce");
        btn_repeat = document.getElementById("btn_repeat");

        if (bounce) {
            btn_bounce.style.filter = "invert(100%)";
            btn_bounce.style.backgroundColor = "white";
            btn_repeat.style.filter = "invert(0%)";
            btn_repeat.style.backgroundColor = "white";
        } else {
            btn_bounce.style.filter = "invert(0%)";
            btn_bounce.style.backgroundColor = "white";
        }
        repeat = false;
    }

    //function enables repeat mode and visualise it by inverting colors.
    function repeatBtn() {
        repeat = !repeat;
        btn_bounce = document.getElementById("btn_bounce");
        btn_repeat = document.getElementById("btn_repeat");

        if (repeat) {
            btn_repeat.style.filter = "invert(100%)";
            btn_repeat.style.backgroundColor = "white";
            btn_bounce.style.filter = "invert(0%)";
            btn_bounce.style.backgroundColor = "white";
        } else {
            btn_repeat.style.filter = "invert(0%)";
            btn_repeat.style.backgroundColor = "white";
        }
        bounce = false;
    }

    /*
    //this function ask for modifications in the animation (assing frames, deleting frames)
    function sendMod(_action, _nr){
        sendFrame(frameList, _nr);
        //toMesh({action: _action, framenr: _nr});
    }
    */

    //send update color of a single led
    function sendFrame(_frameDict, frameToSend = currentFrame) {
        let pixels = [],
            start = parseInt(Object.keys(_frameDict[frameToSend])[0]);
        for (var [ledNr, rgb] of Object.entries(_frameDict[frameToSend])) {
            ledNr = parseInt(ledNr);
            rgb = rgb.split(":").map(Number)
            pixels.push(rgb);

        }
        if (isNaN(start) || pixels.length === 0) return;
        toMesh({"type": "SetLED", "id": getLightboxID(), "start": start, "pixels": pixels});
    }

    //send update colors of a whole frame
    function sendSingleLedJSON(ledNr, leds) {
        toMesh({"type": "SetLED", "id": getLightboxID(), "start": ledNr, "pixels": [leds.split(":").map(Number)]});
    }

    function getFileList() {
        toMesh({action: 'getFileList'});
    }

    function buildFileList(msgdata) {
        console.log("- Building filelist dropdown.");
        msgdata.sort();														//sort the filelist first
        var fileBox = document.getElementById("tb_fileselect");

        fileBox.options.length = 1;	//reset whole list minus 1, leaving only the first option with the text('load animation') available.
        for (var key in msgdata) {	//build option list
            fileBox.options[fileBox.options.length] = new Option(msgdata[key], msgdata[key]);
        }

        for (var i = 0; i < fileBox.options.length; i++) {				//set the default picked option to the chosen savefile
            if (fileBox.options[i].text === currentSaveFile) {
                fileBox.selectedIndex = i;
                break;
            }
        }
    }

    //Function which build a whole dropdown list by a given list of items.
    //writingPipesPool ;  list of all connected lightboxes (taken or not))
    //Writing pipes = list of all connected lightboxes in use (selected by the user)
    //selfPipe = the lightbox assigned to you

    function buildLightboxList(writingPipesPool, writingPipes, selfPipe) {
        console.log("- Building nodelist dropdown.");

        var lbSelector = document.getElementById("tb_lightboxSelect");
        lbSelector.options.length = 0; //reset list
        lbSelector.disabled = false;
        if (selfPipe === null) {																		//if you dont have been assigned a lightbox yet
            if (writingPipesPool.length > 0) {														//and new one are available
                setStatusText("A node got available again!", "normal");								//show popup msg to inform the user
                lbSelector.options[lbSelector.options.length] = new Option("Pick a node", '-1');
                lbSelector.options[0].disabled = true;												//make the 'pick a node' option non clickable
            } else {
                setStatusText("No free nodes available!", "urgent");
                lbSelector.options[lbSelector.options.length] = new Option("No free node!", '-1');	//Show msg in dropdown that no nodes are available.
                lbSelector.options[0].disabled = true;												//make the 'pick a node' option non clickable
            }
        }

        //loop over nodeList and rebuild the dropdown box in html code.
        for (var key in writingPipes) {
            optionString = "Node " + writingPipes[key].toString();

            lbSelector.options[lbSelector.options.length] = new Option(optionString, writingPipes[key]);
            //if not nodes available, disable the dropdown box
            if (writingPipesPool.indexOf(writingPipes[key]) == -1) lbSelector.options[lbSelector.options.length - 1].disabled = true;
            //grayout your currently selected lightbox
            if (writingPipes[key] == selfPipe) lbSelector.options[lbSelector.options.length - 1].disabled = false;
            //Set the default selected option to your own lightbox number
            if (writingPipes[key] == selfPipe) {
                lbSelector.options[lbSelector.options.length - 1].selected = true;
            }
        }
    }

    //Function asks lbController to load an animation file and pass animation data to the browser.
    function loadFile() {
        if (this.selectedIndex == 0) return;
        console.log('LOADING');
        currentSaveFile = this.options[this.selectedIndex].text;
        toMesh({action: 'loadFile', fileName: this.options[this.selectedIndex].text});
    }

    //Function asks lbController to assign a lightbox to current user
    function lightboxSelect() {
        document.getElementById("node").value = getLightboxID();
        //toMesh({action: 'lightboxSelect', lightboxNr: getLightboxID()});

    }

    // choose the serial device
    async function chooseSerialDevice() {
        if (direct_serial) {
            // get new serial device
            const newPort = await navigator.serial.requestPort();
            let portInfo = newPort.getInfo(),
                exists = false,
                dec = new TextDecoder();
            // check if serial device like this exists
            for (let i = 0; i < nodeList.length; i++) {
                let option = nodeList.options[i];
                if (option.textContent === "Device: " + portInfo.usbProductId) {
                    exists = true;
                }
            }

            if (port !== null) port.close();
            port = newPort;
            try {
                // open port with give config
                await port.open({baudRate: 115200, parity: "none", stopBits: 1, dataBits: 8, bufferSize: 2000000});
            } catch (error) {
                // it is already opened
                console.log(error)
            }
            setStatusText("Please wait at least 10 seconds in order to connect to Serial", "urgent");
            await sleep(10000);
            setStatusText("You can use Board", "normal");

            // read node id from serial device
            if (port.readable) {
                const reader = port.readable.getReader();
                try {
                    const {value, done} = await reader.read();
                    if (done) {
                        // Allow the serial port to be closed later.
                        reader.releaseLock();
                    }
                    if (value) {
                        let jsonText = dec.decode(value).match(/{(.*)}/)[1];
                        jsonText = "{" + jsonText + "}"
                        let nodeId = JSON.parse(jsonText)['nodeId'];
                        console.log('nodeId: ' + nodeId);
                        if (!exists) nodeList.add(new Option("Device: " + portInfo.usbProductId, nodeId));
                        nodeList.value = nodeId;
                    }

                } catch (error) {
                    console.log(error);
                }
            }
        }
    }

    // This function is to return dataNr array for assigning ids for led table
    function getDataNrArray(rows, cols) {
        let dataNrArray = new Array(rows),
            dataNr = 1;

        // initialize empty array to store dataNr
        for (let i = 0; i < rows; i++) {
            dataNrArray[i] = new Array(cols);
        }
        switch (direction % 4) {
            case 0:
                for (let r = 0; r < rows; r++) {
                    if (r % 2 === 1) dataNr += cols - 1;
                    else if (r % 2 === 0 && r > 1) dataNr = dataNr + cols + 1;

                    for (let c = 0; c < cols; c++) {
                        dataNrArray[r][c] = dataNr;
                        // increase or decrease
                        dataNr = r % 2 === 0 ? dataNr + 1 : dataNr - 1;
                    }
                }
                break;
            case 1:
                for (let c = cols - 1; c >= 0; c--) {
                    if ((cols - c - 1) % 2 === 0 && c !== cols - 1) dataNr += rows + 1;
                    else if ((cols - c - 1) % 2 === 1) dataNr = dataNr + rows - 1;

                    for (let r = 0; r < rows; r++) {
                        dataNrArray[r][c] = dataNr;
                        // increase or decrease
                        dataNr = (cols - c - 1) % 2 === 0 ? dataNr + 1 : dataNr - 1;
                    }
                }
                break;
            case 2:
                for (let r = rows - 1; r >= 0; r--) {
                    // check it is odd or even, to add ids like a zigzag
                    if ((rows - r - 1) % 2 === 1) dataNr += cols - 1;
                    else if ((rows - r - 1) % 2 === 0 && r !== rows - 1) dataNr = dataNr + cols + 1;

                    for (let c = cols - 1; c >= 0; c--) {
                        dataNrArray[r][c] = dataNr;
                        // increase or decrease
                        dataNr = (rows - r - 1) % 2 === 0 ? dataNr + 1 : dataNr - 1;
                    }
                }
                break;
            case 3:
                for (let c = 0; c < cols; c++) {
                    if (c % 2 === 0 && c !== 0) dataNr += rows + 1;
                    else if (c % 2 === 1) dataNr = dataNr + rows - 1;

                    for (let r = rows - 1; r >= 0; r--) {
                        dataNrArray[r][c] = dataNr;
                        // increase or decrease
                        dataNr = c % 2 === 0 ? dataNr + 1 : dataNr - 1;
                    }
                }
                break;

        }

        return dataNrArray;
    }

    //Function to change the led grid when using non standard 9*5 lightbox.
    //The hole ledfield gets rebuild, incuding the ledDirection array.
    //The function build the same html strucuture as before with other domensions.
    //Only used when the user changes the layout or request a layout because its
    //saved in the cookie as default.
    function dimensionBtn(rows, cols, numLeds, ledDirection, startEmpty) {
        // transfer it into integers
        if (direction % 4 === 0 || direction % 4 === 2) {
            rows = parseInt(rows);
            cols = parseInt(cols);
        } else {
            // exchange variables
            rows = parseInt(cols) + parseInt(rows);
            cols = rows - parseInt(cols);
            rows = rows - cols;
        }

        let div = document.getElementById("ledview");
        while (div.firstChild) {
            div.removeChild(div.firstChild);						//remove whole led grid.
        }

        let MAX_LEDS = numLeds,
            dataNr = 1,													//lednumbering in html code starts from 1
            dataNrArray = getDataNrArray(rows, cols),
            htmlString = "",
            english_letters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"];


        //toprow with numbering
        htmlString += '<div id="ledview_fly" 	class="box ledview_toprow"><img class="fflogoimg_inv" src="img/fireflylogo_inv.png"></div>';
        for (let c = 1; c <= cols; c++) {
            // to decide what to write on top row
            let topRow = '';
            switch (direction % 4) {
                case 0:
                    topRow = c;
                    break;
                case 1:
                    topRow = english_letters[cols - c];
                    break;
                case 2:
                    topRow = cols - c + 1;
                    break;
                case 3:
                    topRow = english_letters[c - 1];
            }

            htmlString += `<div id="ledtop_${c}" class="box ledview_toprow">${topRow}</div>`;
        }
        //build the led grid view
        for (let r = 1; r <= rows; r++) {
            // to decide what to write on left column
            let leftColumn = '';
            switch (direction % 4) {
                case 0:
                    leftColumn = english_letters[r - 1];
                    break;
                case 1:
                    leftColumn = r
                    break;
                case 2:
                    leftColumn = english_letters[rows - r];
                    break;
                case 3:
                    leftColumn = rows - r + 1;
            }

            htmlString += `<div class="box ledview_leftcol">${leftColumn}</div>`;
            for (let c = 1; c <= cols; c++) {
                if (dataNr <= numLeds)
                    htmlString += `<div id="ledview_${dataNrArray[r - 1][c - 1]}" class="ledview_bg box boxed_led" data-nr="${dataNrArray[r - 1][c - 1]}"></div>`;
                else
                    htmlString += `<div class="box"></div>`;		// if ledCount < rows * cols : add empty box to make grid fit
            }
        }

        //previewrow
        htmlString += '<div id="preview_P" 	class="box ledview_leftcol"></div>';
        for (let c = 1; c <= cols; c++) {
            htmlString += `<div id="preview_${c}" class="box boxed_preview" data-nr="${c - 1}">&nbsp;</div>`;
        }

        // (ledrows - 1) * 10 we are substracting by this because of margins that each row has
        let wrapperDivHeight = document.getElementById('wrapper').clientHeight - 70 - (rows) * 5,
            ledViewDivWidth = screen.availWidth - document.getElementById('left_panel').clientWidth - document.getElementById("tool_box").clientWidth - 70 - (cols - 1) * 5,
            // calculate length by each
            squareLengthByHeight = wrapperDivHeight / rows,
            squareLengthByWidth = ledViewDivWidth / cols,
            // compare which one is smaller to set that one
            squareLength = squareLengthByHeight < squareLengthByWidth ? squareLengthByHeight : squareLengthByWidth;


        div.style.gridTemplateColumns = `40px repeat(${cols}, ${squareLength}px)`;
        div.style.gridTemplateRows = `40px repeat(${rows}, ${squareLength}px) 30px`;

        div.innerHTML += htmlString;
        //reinitialise framelist
        if (startEmpty) {					//start with an empty frameList or keep the existing one
            frameList = [];
            frameList[0] = {}; 		//create first frame as dictionary
            for (var i = 0; i < MAX_LEDS; i++) {
                frameList[0][i] = "0:0:0"
            }

            frameLayerList = [];
            frameLayerList[0] = []


            for (let i = 0; i < numberOfLayers; i++) {
                frameLayerList[0][i] = {}
                for (let j = 0; j < MAX_LEDS; j++) {
                    frameLayerList[0][i][j] = "0:0:0"
                }
            }
            toMesh({action: 'newdimension', numLeds: numLeds, rows: rows, cols: cols, ledDirection: ledDirection});
        }

        firstFrame();			//goto first frame and display it
        attachLedActions();		//you must reattach the functions after rebuilding all LED grid boxes.
    }

    function saveOrUploadModal() {
        let saveOrLoadModal = document.getElementById("saveOrLoadModal"),
            today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate(),
            time = today.getHours() + "h" + today.getMinutes() + "m" + today.getSeconds() + "s",
            dateTime = date + ' ' + time;
        saveOrLoadModal.style.display = 'block';
        document.getElementById('save-file-name').value = 'firefly' + dateTime
        document.getElementById("chooseFileDivId").innerHTML = 'Select File ...';
        document.getElementById("updateLedBox").checked = true;
        document.getElementById("updateColors").checked = true;
    }

    function saveFile() {
        let filename = document.getElementById('save-file-name').value,
            dlAnchorElem = document.createElement("a");

        let colors = {};

        // collect colors from colorbox
        colors['select'] = window.getComputedStyle(document.getElementById('tb_colorselect')).getPropertyValue("background-color");
        for (let i = 1; i <= 10; i++) {
            let colorBox = document.getElementById('tb_color_' + i);
            colors[i.toString()] = window.getComputedStyle(colorBox).getPropertyValue("background-color");
        }

        let downloadObject = {
                'animationSpeed': document.getElementById('speedSlider').value,
                'colors': colors, 
                'frameList': frameList, 
                'frameLayerList': frameLayerList
            },
            dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(downloadObject));

        dlAnchorElem.setAttribute("href", dataStr);
        dlAnchorElem.setAttribute("download", filename.endsWith(".json") ? filename : filename + ".json");
        dlAnchorElem.click();

        document.getElementById("saveOrLoadCloseBtn").click();
        setStatusText("File Saved Successfully", "normal");
    }

    // to control when it was clicked and to prevent multiple clicking
    let uploadFileClickTime = new Date();

    function uploadFile() {
        if ((new Date()).getTime() - uploadFileClickTime.getTime() < 2000) return;

        console.log('upload file clicked');
        uploadFileClickTime = new Date();
        let import_button = document.getElementById('import-file');

        import_button.value = null;
        import_button.click();
        import_button.onchange = function (e) {
            e.preventDefault();

            let importedFile = e.target.files[0];
            document.getElementById("chooseFileDivId").innerHTML = importedFile.name;
        }
    }

    function submitUploadForm() {
        let button = document.getElementById('import-file'),
            file = button.files[0];

        if (file === undefined) {
            alert("Please Select File");
            return;
        }

        let reader = new FileReader();
        reader.onload = function () {
            let uploadedFile = JSON.parse(reader.result),
                colors = uploadedFile['colors'],
                updateLedBox = document.getElementById("updateLedBox").checked,
                updateColors = document.getElementById("updateColors").checked;

            if (updateColors) {
                // set colors from file
                document.getElementById('tb_colorselect').style.backgroundColor = colors['select'];
                for (let i = 1; i <= 10; i++) {
                    document.getElementById('tb_color_' + i).style.backgroundColor = colors[i];
                }
            }

            if (updateLedBox) {
                frameList = uploadedFile['frameList'];
                frameLayerList = uploadedFile['frameLayerList'];
                sendFrame(frameList, 0);
                renderViewMaster();
                renderLeds();
                firstFrame();
            }

            const scale = (num, in_min, in_max, out_min, out_max) => {
                return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
            }
            document.getElementById("speedSlider").value = parseInt(uploadedFile['animationSpeed']);
            num = parseInt(uploadedFile['animationSpeed']);
            
            // changed the output speed to 50
            scaledValue = Math.round(scale(num, 0, 255, 0, 5000)); //speed is send as scaled value 0-255
            currentSpeed = scaledValue;
            // document.getElementById("speedSliderMembank").value = num; 		//duplicate speed to membankspeed slider
            // document.getElementById("sliderValueMembank").innerHTML = scaledValue + " ms";
            document.getElementById("tb_framespeed_title").innerHTML = "SPEED:" + scaledValue + "ms";


        };
        reader.readAsText(file);

        setStatusText("File Loaded Successfully", "normal");

        document.getElementById("saveOrLoadCloseBtn").click();
    }

    async function MemorySave() {
        //https://codepen.io/Hebe/details/KddqNa/ for button progress bar
        stop(); 																//stop current play of the animation to stop ongoing websocket traffic.
        console.log('SAVING to memory');
        let bank_e = document.getElementById("commandBank"),
            flow_e = document.getElementById("commandFlow");
        autoplay = (document.querySelector('input[name="autoplay"]:checked').value === "yes") ? 1 : 0;
        playback = parseInt(flow_e.options[flow_e.selectedIndex].value);	//sends the literal text, not 1 or 0
        bank = parseInt(bank_e.options[bank_e.selectedIndex].value);
        speed = parseInt(document.getElementById("commandSpeed").value);


        var pixels = [];
        var start = 0;

        for (const frame of frameList) {
            for (let [ledNr, rgb] of Object.entries(frame)) {
                ledNr = parseInt(ledNr);
                rgb = rgb.split(":").map(Number)
                pixels.push(rgb);
            }
        }

        if (isNaN(start) || pixels.length === 0) return;

        document.getElementById("saveOrSendCommandBtn").textContent = "Saving";
        document.getElementById("saveOrSendCommandBtn").disabled = true;
        /*
        background-image: repeating-linear-gradient(-45deg, transparent, transparent 20px, rgba(0, 200, 0, 0.5) 20px, rgba(0, 200, 0, 0.5) 40px);
    background-repeat: no-repeat;
      background-size: 20% 100%;
        */

        let index = 0;
        while (index < pixels.length) {
            let pixelChunk = pixels.slice(index, 8 + index);
            toMesh({"type": "SaveAnimation", "id": getLightboxID(), "start": index, "memorybank": bank, "pixels": pixelChunk});
            index += 8;
            await sleep(2000);
        }

        toMesh({
            "type": "ConfigureAnimation",
            "id": getLightboxID(),
            "memorybank": bank,
            "autoplay": autoplay,
            "ledsPerFrame": MAX_LEDS,
            "frameCount": frameList.length,
            "speed": speed,
            "flow": playback,
            "repeat": 0
        });

        document.getElementById("saveOrSendCommandBtn").textContent = "Save";
        document.getElementById("saveOrSendCommandBtn").disabled = false;

        /*
            duration = frameList.length * 1600; //1600 is time per frame needed approx to save a frame. Change to make it more accurate.
            let rule = `.is-loading {  animation: progressFill ${duration}ms -0.1s ease-in-out forwards;}`;
            addCss(rule);	//you always have to add the rule to be abe to reactive it again.
            console.log(rule);
            document.getElementById("memSaveBtn").disabled=true;					//disable click during saving
            document.getElementById("memSaveBtn").classList.add("is-loading");		//activate animation
            document.getElementById("memSaveBtn").value="Saving";

            //This timer gets execute after precalculated time to remove the animation and reset the text.
            setTimeout(function(){
                document.getElementById("memSaveBtn").classList.remove("is-loading");
                document.getElementById("memSaveBtn").value="Save";
                document.getElementById("memSaveBtn").disabled=false;
            }, duration+500);
            */
    }

    /* OK */

    //function update the gui text with the interval in ms.
    function speedSliderMembankChange() {
        console.log("memspeed change");
        const scale = (num, in_min, in_max, out_min, out_max) => {
            return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
        num = document.getElementById("speedSliderMembank").value;
        document.getElementById("sliderValueMembank").innerHTML = Math.round(scale(num, 0, 255, 30, 2000)) + " ms";
    }

    /* OK */
    //Change to speed of the currently playing animation
    //Function executed when slinding the speed slider and change the gui elements together.
    //The speedSliderMouseUp() is triggered when letting go the mousebutton and it that function which send the actual speed to lbController.
    //This function only does the gui elements, otherwise a speed change is send on every little move.
    function speedSliderChange() {
        const scale = (num, in_min, in_max, out_min, out_max) => {
            return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }
        num = document.getElementById("speedSlider").value;

        // changed the output speed to 50
        scaledValue = Math.round(scale(num, 0, 255, 0, 5000)); //speed is send as scaled value 0-255
        currentSpeed = scaledValue;
        // document.getElementById("speedSliderMembank").value = num; 		//duplicate speed to membankspeed slider
        // document.getElementById("sliderValueMembank").innerHTML = scaledValue + " ms";
        document.getElementById("tb_framespeed_title").innerHTML = "SPEED:" + scaledValue + "ms";
    }


    //only gets called when release mouse so you only send speed update once.
    function speedSliderMouseUp() {
        if (playing) {
            clearInterval(animation);
            animation = setInterval(player, currentSpeed);
        }
        /*
        console.log("sending speed update");

        const scale = (num, in_min, in_max, out_min, out_max) => {
            return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
        }

        num = document.getElementById("speedSlider").value;
        scaledValue = Math.round(scale(num, 0, 255, 200, 2000));		//calculated on scale of 2000 ms
        lightbox = document.getElementById("tb_lightboxSelect");

        toMesh({action: 'speed', lightboxNr: getLightboxID(), speedValue : scaledValue});
        */
    }

    /* OK */

    //function to change the # of rows and cols of the animation (if using different shaped leds boxes)
    function applyChanges() {
        // send blinker settings
        sendBlinker = document.getElementById('sendBlinker').checked;

        //Function to redraw all led fields with its color value.
        if (document.getElementById('showCommands').checked) document.getElementById("brython_test_fieldset").style.display = 'block';
        else document.getElementById("brython_test_fieldset").style.display = 'none';


        // check direction
        direction = parseInt(document.querySelector('input[name="rotate"]:checked').value);


        console.log('change matrix');
        let newRows = document.getElementById("defaultRows"),
            newCols = document.getElementById("defaultCols"),
            newLeds = document.getElementById("defaultLeds"),
            newLedDirection = document.getElementById("ledDirection");		//is the ledstrip continuous or not
        let startEmpty = newRows.value !== ledRows || newCols.value !== ledCols;
        ledRows = newRows.value;
        ledCols = newCols.value;
        MAX_LEDS = newLeds.value;
        //input validation
        if (newRows.value === "" || newCols.value === "") {
            document.getElementById("settingsErrorLabel").innerHTML = "Fields cannot be empty!";
            return;
        }
        //ask lbController to create new animation
        dimensionBtn(newRows.value, newCols.value, newLeds.value, newLedDirection.checked, startEmpty);
        //hide window
        settingsModal.style.display = "none";
    }

    /* OK */
    function saveDefaults() {	//this function stores the default led animation settings in a cookie.
        // newName = document.getElementById("defaultUsername");		//default name of connection user.
        newRows = document.getElementById("defaultRows");			//default number of rows of new animation
        newCols = document.getElementById("defaultCols");			//default number of cols of new animation
        newLeds = document.getElementById("defaultLeds");			//default number of LEDS

        //input validation
        if (newName.value == "" || newRows.value == "" || newCols.value == "") {
            document.getElementById("settingsErrorLabel").innerHTML = "Fields cannot be empty!";
            return;
        }

        //set cookie values
        // document.cookie = 'userName=' + newName.value + '; path=/';	//mind the space between; and path. This is mandatory for some browsers .
        document.cookie = 'defaultRows=' + newRows.value + '; path=/';
        document.cookie = 'defaultCols=' + newCols.value + '; path=/';
        document.cookie = 'defaultLeds=' + newLeds.value + '; path=/';
        console.log('Saved new defaults in cookie.') + '; path=/';
        settingsModal.style.display = "none";							//hide window
    }

    /* OK */
//all keybord shortcut actions are defined here.
    document.addEventListener('keydown', function (event) {
        switch (event.keyCode) {
            case 37:			//left
                previous();
                break;
            case 39:			//right
                next();
                break;
            case 80:			//p
                play();
                break;
            case 83:
                stop();			//s
                break;
            case 45:
                insertFrame(); 	//insert
                break;
            case 46:
                deleteFrame();	//delete
                break;
            case 67:
                copy();
                break;			//c
            case 86:
                paste();  		//v
                break;
            case 36:
                firstFrame(); 	//home
                break;
            case 33:
                firstFrame();  	//end
                break;
            case 34:
                lastFrame();  	//pageup
                break;
            case 35:
                lastFrame(); 	//pagedown
                break;
            case 82:
                repeatBtn(); 	//r
                break;
            case 66:
                bounceBtn(); 	//b
                break;
            case 84:			//t - cycle through available tools
                if (selectedDrawTool === document.getElementById("btn_pencil")) {
                    document.getElementById("btn_filler").click();
                    break;
                }
                if (selectedDrawTool === document.getElementById("btn_filler")) {
                    document.getElementById("btn_eyedropper").click();
                    break;
                }
                if (selectedDrawTool === document.getElementById("btn_eyedropper")) {
                    document.getElementById("btn_pencil").click();
                    break;
                }
                break;
            case 40:			//down
                colorPointer += 1;
                if (colorPointer > 11) colorPointer = 1;
                document.getElementById("tb_color_" + colorPointer).click();
                break;
            case 38:			//up
                colorPointer -= 1;
                if (colorPointer < 1) colorPointer = 11;
                document.getElementById("tb_color_" + colorPointer).click();
                break;
            case 69:			//e eraserall
                clearAll();
                break;
            case 68:			//future use. download saved json file to your pc.
                console.log("downloading animation");
                break;
            case 27: // ESC
                closeActiveWindows();
                break;
            default:
            //console.log("unknown keypress.") document.getElementById("btn_pencil").click();
        }
    });


    function closeActiveWindows() {
        let modals = document.getElementsByClassName('modal');
        for (let i = 0; i < modals.length; i++) {
            if (modals[i].style.display === 'block') modals[i].style.display = 'none';
        }
    }

    function sendCommand() {
        let data = {},
            option_e = document.getElementById("commandType"),
            bank_e = document.getElementById("commandBank");

        data["id"] = document.getElementById("node").value;
        data["type"] = option_e.options[option_e.selectedIndex].value;
        data["memorybank"] = parseInt(bank_e.options[bank_e.selectedIndex].value);
        data["speed"] = document.getElementById("commandSpeed").value;
        data["ledsPerFrame"] = MAX_LEDS;
        data["frameCount"] = frameList.length;
        toMesh(data);
    }

    function configureAnimation() {
        let autoplay = 1,
            bank_e = document.getElementById("commandBank"),
            flow_e = document.getElementById("commandFlow"),
            playback = parseInt(flow_e.options[flow_e.selectedIndex].value),	//sends the literal text, not 1 or 0
            bank = parseInt(bank_e.options[bank_e.selectedIndex].value),
            speed = parseInt(document.getElementById("commandSpeed").value);

        toMesh({
            "type": "ConfigureAnimation",
            "id": getLightboxID(),
            "memorybank": bank,
            "autoplay": autoplay,
            "ledsPerFrame": MAX_LEDS,
            "frameCount": frameList.length,
            "speed": speed,
            "flow": playback,
            "repeat": 0, //????
            "output": 1 //????
        });
    }

    // this function is for commands, sending or saving them.
    async function saveOrSendCommand() {
        let buttonType = document.getElementById("saveOrSendCommandBtn").textContent,
            option_e = document.getElementById("commandType");
        if (buttonType === "Save") await MemorySave();
        else if (buttonType === "Send") {
            // if we are gonna configure animation we have separate function for that, and separate funciton for sending command
            if (option_e.options[option_e.selectedIndex].value === 'ConfigureAnimation') configureAnimation()
            else sendCommand();
        }
    }

    //this function is to change the form according to the option we choose for the command
    document.getElementById('commandType').addEventListener('change', function (event) {
        // we are gonna hide or show those components according to the option
        let componentIds = ["commandBankTitle", "commandBank", "commandSpeedTitle", "commandSpeed", "commandSpeedSpan", "commandFlowTitle", "commandFlow"],
            action = this.value === "StopAnimation" || this.value === "ClearLED" ? "none" : "inline";

        document.getElementById("saveOrSendCommandBtn").textContent = this.value === "SaveAnimation" ? "Save" : "Send";

        for (let i = 0; i < componentIds.length; i++) {
            document.getElementById(componentIds[i]).style.display = action;
        }

        if (event.currentTarget.value === 'SaveAnimation') document.getElementById("autoplay_div_id").style.display = 'inline'
        else document.getElementById("autoplay_div_id").style.display = 'none';

    });


    async function toMesh(_data) {
        if (direct_serial) {
            brythonListener(JSON.stringify(_data))
            // we have returned that value to localstorage from brython, now we are getting it into javascript
            let meshed_message = JSON.parse(localStorage.getItem('meshed_message')),
                enc = new TextEncoder();
            
            if (port !== null) {
                for (let inx in meshed_message) {
                    let encoded_message = enc.encode(meshed_message[inx]).subarray();
                    //let encoded_message = enc.encode(currentMeshedMessage).subarray();
                    try {
                        const writer = port.writable.getWriter();
                        await writer.write(encoded_message);
                        await writer.releaseLock();
                    } catch (error) {
                        console.log(error);
                        const writer = port.writable.getWriter();
                        await writer.releaseLock();
                    }
                }
            }
        } else {
            if (_data.hasOwnProperty("id") && _data.id == -1) return; //Don't msg to unexisting nodes
            var data = new URLSearchParams();
            _data = JSON.stringify(_data);
            data.append("json", _data);
            fetch('/post', {method: 'post', body: data});
            console.log("[toMesh] <" + "-- " + _data);
        }
    }

    async function init() {
        // init brython
        await brython(1);

        // set height and width of leds
        let wrapperDivHeight = document.getElementById('wrapper').clientHeight - 110,
            squareLength = wrapperDivHeight / ledRows,
            div = document.getElementById("ledview");

        div.style.gridTemplateColumns = `40px repeat(${ledCols}, ${squareLength}px)`;
        div.style.gridTemplateRows = `40px repeat(${ledRows}, ${squareLength}px) 30px`;

        if (direct_serial) {
            // if it is directly connected to serial
            if ("serial" in navigator) {
                setStatusText("Choose Serial To Connect", "normal");
                nodeList.options[nodeList.options.length] = new Option("Choose Serial", '-1'); // Initial value
            } else {
                // serial is not supported by browser
                alert('Serial is not supported by this browser')
                nodeList.options[nodeList.options.length] = new Option("No serial detected!", '-1'); // Initial value
            }
        } else {
                // TODO
                var source = new EventSource('/stream?channel=fromMesh');
                source.onmessage = function (e) {
                    var data = JSON.parse(e.data);
                    if (data.hasOwnProperty('type')) {
                        switch (data.type) {
                            case 'connect':
                            case 'pong':
                                addNodeID(data.node);
                                break;
                            case 'disconnect':
                                removeNodeID(data.node);
                                break;

                        }
                    }


                    console.log("[fromMesh] --" + "> " + e.data);
                };
                source.onopen = function (e) {
                    setStatusText("Connected", "normal");
                };
                source.onerror = function (e) {
                    setStatusText("Not connected", "urgent");
                };
                // if it is run on raspberry
                // nodeList.options[nodeList.options.length] = new Option("No free node!", '-1'); // Initial value
        }
            


        // ??? need to find better way to check about browser
        if (direct_serial) {
            if (!navigator.userAgent.match(/chrome|chromium|crios/i)) alert("please use the chrome in order to use Serials");
        }
    }

    //  ------------------------------------------- COLOR PICKER -------------------------------------------------------------

    // added color picker
    const popupBasic = new Picker({
        parent: document.querySelector('#btn_colorpick'),
        alpha: false // default: true
    });


    // time to control send intervals
    let colorPickerTime = new Date(),
        pickingColor = false;

    // on color change set first 3 led with that color
    popupBasic.onChange = function (color) {
        pickingColor = true;

        let timeDiff = (new Date()).getTime() - colorPickerTime.getTime(); // it is in ms
        if (timeDiff > 30) {
            // update time
            colorPickerTime = new Date();

            let rgb_color = color.rgba.slice(0, -1),
                // copy frame list
                pixel_array = frameList.slice();
            for (let i = 0; i < 3; i++) {
                pixel_array[i] = rgb_color;
            }
            toMesh({"type": "SetLED", "id": getLightboxID(), "start": 0, "pixels": pixel_array});
            document.querySelector('#tb_colorselect').style.backgroundColor = color.rgbString;
        }
    };

    // on close set current frame on device
    popupBasic.onClose = function () {
        pickingColor = false;
        sendFrame(frameList, currentFrame);
    };

    // on close set current frame on device
    popupBasic.onDone = function () {
        pickingColor = false;
        sendFrame(frameList, currentFrame);
    };

    // attach drag events to color selector
    document.getElementById("tb_colorselect").addEventListener('dragstart', drag)
    document.getElementById("tb_colorselect").addEventListener('dragend', preventMouseDown)
    document.getElementById("tb_colorselect").addEventListener('dragexit', preventMouseDown)

    // attach on drop listeners to all colors
    for (let i = 1; i < 11; i++) {
        let colorId = 'tb_color_' + i
        document.getElementById(colorId).addEventListener('drop', dropColor);
        document.getElementById(colorId).addEventListener('dragover', allowDrop);
        document.getElementById(colorId).addEventListener('dragenter', allowDrop);
    }

    // by default javascript does not allow droping, so we are preventing default settings
    function allowDrop(event) {
        event.preventDefault();
    }

    // when drag starts, we need to pass the color that we want to change
    function drag(event) {
        event.dataTransfer.setData("text", event.target.id);
        event.dataTransfer.setData("color", event.currentTarget.style.backgroundColor);
    }

    // drop function for colors
    function dropColor(event) {
        event.preventDefault(event);
        let color = event.dataTransfer.getData('color');
        event.currentTarget.style.setProperty('background', color);
        mouseDown = false;
    }

    // after drag mouse stays clicked, this function is to prevent that
    function preventMouseDown() {
        mouseDown = false;
    }

    // ----------------------------------------------------------- LAYERS -------------------------------------------------------------------

    // attach listeners
    let layerEnableCheckboxes,
        layerEditCheckboxes,
        layerNames;

    function attachLayerListeners() {
        layerEnableCheckboxes = document.getElementsByClassName('layer-enable-checkbox-class');
        layerEditCheckboxes = document.getElementsByClassName('layer-edit-checkbox-class');

        for (let i = 0; i < layerEnableCheckboxes.length; i++) {
            layerEditCheckboxes[i].addEventListener('change', selectLayer);
        }


        let gridItemComponents = document.getElementsByClassName('enable-layer-div-class');
        for (let i = 0; i < gridItemComponents.length; i++) {
            gridItemComponents[i].addEventListener('click', enableLayer);
        }

        layerNames = document.getElementsByClassName('layer-name-class');
        for (let i = 0; i < layerNames.length; i++) {
            layerNames[i].addEventListener('click', editLayerName)
        }
    }

    attachLayerListeners();
    document.getElementById('add_layer_id').addEventListener('click', addLayer)
    document.getElementById('remove_layer_id').addEventListener('click', removeLayer)

    activeLayerForNameChange = -1;

    function editLayerName() {
        let editLayerNameModal = document.getElementById("editLayerNameModal");
        editLayerNameModal.style.display = 'block';

        layerRank = parseInt(this.previousElementSibling.previousElementSibling.previousElementSibling.innerHTML);
        document.getElementById('layer-name-input').value = frameLayerNames[layerRank];

        activeLayerForNameChange = layerRank;

    }


    function saveLayerName(){
        layerNewName = document.getElementById('layer-name-input').value;
        frameLayerNames[activeLayerForNameChange] = layerNewName;
        document.getElementById(`layer_name_${activeLayerForNameChange}`).innerHTML = layerNewName;

        document.getElementById("editLayerNameModal").style.display = 'none';
    }


    // this is listener for clicking layers
    function selectLayer() {
        // turn all checkboxes to false
        for (let i = 0; i < layerEditCheckboxes.length; i++) {
            layerEditCheckboxes[i].checked = false;

            // if we have disabled layers, remove disability
            if (layerEnableCheckboxes[i].disabled) {
                layerEnableCheckboxes[i].checked = false;
                layerEnableCheckboxes[i].disabled = false;
            }
        }
        // enable current layer
        this.checked = true;

        // update current layer and change color
        currentLayer = parseInt(this.id.split('_').at(-1));
        if (!frameEnabledLayers[currentLayer]) {
            let currentEnabledLayerCheckbox = document.getElementById(`layer_enabled_${currentLayer}`);
            currentEnabledLayerCheckbox.checked = true;
            currentEnabledLayerCheckbox.disabled = true;
            currentEnabledLayerCheckbox.readOnly = false;
        }
        updateFrameList();
    }

    // this is for enabling layers
    function enableLayer() {
        let checkbox = this.parentNode.firstElementChild,
            layerIndex = parseInt(checkbox.id.split("_").at(-1)),
            checked = !frameEnabledLayers[layerIndex];

        if (currentLayer === layerIndex) {
            checkbox.disabled = !checked;
        } else {
            checkbox.checked = checked;
        }

        frameEnabledLayers[layerIndex] = checked;
        updateFrameList();
    }

    function addLayer() {
        let layerGrid = document.getElementById('layer_component'),
            newLayerIndex = (layerGrid.children.length - 4) / 4     ,
            lastElement = layerGrid.children[4],
            gridRankItem = createLayerGridDiv(),
            gridEnableItem = createLayerGridDiv(),
            gridEditItem = createLayerGridDiv();
            girdLayerNameItem = createLayerGridDiv(),
            newLayerName = `Layer-${newLayerIndex}`;

        // create complete divs
        gridRankItem.innerHTML = `${newLayerIndex}`;
        gridEnableItem.innerHTML = `<input type="checkbox" id="layer_enabled_${newLayerIndex}" class="layer-enable-checkbox-class" checked>
                                    <div class="enable-layer-div-class"></div>`;
        gridEditItem.innerHTML = `<input type="checkbox" id="layer_editable_${newLayerIndex}" class="layer-edit-checkbox-class">`

        girdLayerNameItem.innerHTML = newLayerName;
        girdLayerNameItem.classList.add("layer-name-class");
        girdLayerNameItem.id = `layer_name_${newLayerIndex}`;
        
        frameLayerNames[newLayerIndex] = newLayerName;

        // append divs
        layerGrid.insertBefore(gridRankItem, lastElement);
        layerGrid.insertBefore(gridEnableItem, lastElement);
        layerGrid.insertBefore(gridEditItem, lastElement);
        layerGrid.insertBefore(girdLayerNameItem, lastElement);

        // update variables
        for (let i = 0; i < frameLayerList.length; i++) {
            frameLayerList[i][newLayerIndex] = {};
            for (let j = 0; j < MAX_LEDS; j++) {
                frameLayerList[i][newLayerIndex][j] = "0:0:0";
            }
        }
        frameEnabledLayers[newLayerIndex] = true;

        // inc number of layers
        numberOfLayers += 1;

        // reattach listeners
        attachLayerListeners();
    }


    function createLayerGridDiv() {
        let layerGridDiv = document.createElement("div");
        layerGridDiv.setAttribute("class", "grid-item");
        return layerGridDiv;
    }

    function removeLayer() {
        // if there is one layer left prevent removing
        if (layerEditCheckboxes.length === 1) {
            setStatusText("Can not remove Last Layer", "normal");
            return;
        }

        if (confirm('Are you sure you want to remove the layer?')) {
            let layerGrid = document.getElementById('layer_component');

            // remove that row
            let childrenTargetIndex = layerGrid.children.length - 4 - currentLayer * 4;
            for (let i = 0; i < 4; i++) {
                layerGrid.removeChild(layerGrid.children[childrenTargetIndex]);
            }

            // reattach listeners
            attachLayerListeners();

            
            // overwrite new ids for components after removed row
            for (let i = currentLayer; i < layerEditCheckboxes.length; i++) {
                let gridItemIndex = layerGrid.children.length - 4 - i * 4;

                layerGrid.children[gridItemIndex].innerHTML = `${i}`;
                layerGrid.children[gridItemIndex + 1].firstElementChild.id = `layer_enabled_${i}`;
                layerGrid.children[gridItemIndex + 2].firstElementChild.id = `layer_editable_${i}`;
                layerGrid.children[gridItemIndex + 3].id = `layer_name_${i}`;
            }

            // update variables
            frameLayerList[currentFrame].splice(currentLayer, 1);


            for (let i = currentLayer; i < Object.keys(frameEnabledLayers).length - 1; i++) {
                frameEnabledLayers[i] = frameEnabledLayers[i + 1];
                frameLayerNames[i] = frameLayerNames[i + 1];
            }
            delete frameEnabledLayers[Object.keys(frameEnabledLayers).length - 1];
            delete frameLayerNames[Object.keys(frameLayerNames).length - 1];

            currentLayer = currentLayer === 0 ? 0 : currentLayer - 1;

            document.getElementById(`layer_editable_${currentLayer}`).click();

            numberOfLayers -= 1;

        }

    }   

    
    function duplicateLayer(){
        let layerGrid = document.getElementById('layer_component'),
            newLayerIndex = currentLayer + 1,
            currentLastIndex = layerGrid.children.length - 4 - currentLayer * 4,
            childrenTargetIndex = layerGrid.children[currentLastIndex],
            gridRankItem = createLayerGridDiv(),
            gridEnableItem = createLayerGridDiv(),
            gridEditItem = createLayerGridDiv();
            girdLayerNameItem = createLayerGridDiv(),
            newLayerName = document.getElementById(`layer_name_${currentLayer}`).innerHTML;

        // create complete divs
        gridRankItem.innerHTML = `${newLayerIndex}`;
        gridEnableItem.innerHTML = `<input type="checkbox" id="layer_enabled_${newLayerIndex}" class="layer-enable-checkbox-class" checked>
                                    <div class="enable-layer-div-class"></div>`;
        gridEditItem.innerHTML = `<input type="checkbox" id="layer_editable_${newLayerIndex}" class="layer-edit-checkbox-class">`

        girdLayerNameItem.innerHTML = newLayerName;
        girdLayerNameItem.classList.add("layer-name-class");
        girdLayerNameItem.id = `layer_name_${newLayerIndex}`;

        // append divs
        layerGrid.insertBefore(gridRankItem, childrenTargetIndex);
        layerGrid.insertBefore(gridEnableItem, childrenTargetIndex);
        layerGrid.insertBefore(gridEditItem, childrenTargetIndex);
        layerGrid.insertBefore(girdLayerNameItem, childrenTargetIndex);
        
        // duplicate frame names and enabled layers, add one more layer for each
        let placeHolderForName = frameLayerNames[currentLayer],
            placeHolderForEnabledLayer = frameEnabledLayers[currentLayer];

        for (let i = currentLayer; i < numberOfLayers; i++) {
            let tempName = frameLayerNames[i+1],
                tempEnabledLayer = frameEnabledLayers[i+1];

            frameLayerNames[i+1] = placeHolderForName;
            frameEnabledLayers[i+1]= placeHolderForEnabledLayer;

            placeHolderForName = tempName;
            placeHolderForEnabledLayer = tempEnabledLayer;
            
        }

        // overwrite new ids for components after duplicating row
        for (let i = currentLayer + 2; i < layerEditCheckboxes.length; i++) {
            let gridItemIndex = layerGrid.children.length - 4 * (i + 1);

            layerGrid.children[gridItemIndex].innerHTML = `${i}`;
            layerGrid.children[gridItemIndex + 1].firstElementChild.id = `layer_enabled_${i}`;
            layerGrid.children[gridItemIndex + 2].firstElementChild.id = `layer_editable_${i}`;
            layerGrid.children[gridItemIndex + 3].id = `layer_name_${i}`;
        }

        // update variables
        frameLayerList[currentFrame].splice(currentLayer, 0, JSON.parse(JSON.stringify(frameLayerList[currentFrame][currentLayer])));

        // increase the current layer by 1
        currentLayer += 1;
        numberOfLayers += 1;

        // reattach listeners
        attachLayerListeners();

        // click duplicated layer 
        document.getElementById(`layer_editable_${currentLayer}`).click();

    }


    function updateFrameList(includeSelectedLayer = true) {
        // empty frameList
        for (let i = 0; i < MAX_LEDS; i++) {
            frameList[currentFrame][i] = "0:0:0";
        }

        // update frameList
        for (let i = numberOfLayers - 1; i >= 0; i--) {
            if (frameEnabledLayers[i] || (i === currentLayer && includeSelectedLayer))
                for (let j = 0; j < MAX_LEDS; j++) {
                    if (frameList[currentFrame][j] === "0:0:0")
                        frameList[currentFrame][j] = frameLayerList[currentFrame][i][j];
                }
        }

        sendFrame(frameList, currentFrame);
        renderViewMaster();
        renderLeds();
    }


    //-----------------------------------------------LAYERS DRAG--------------------------------------------------
    
    function layerMoveDownUp(){
        let direction = this.id.split("_")[1] === "down" ? -1 : 1;

        // direction we are dragging up or down
        if (direction == -1){
            if (currentLayer == 0 ){
                return; 
            }    
        } else if (direction == 1) {
            if (currentLayer == numberOfLayers - 1 ){
                return;
            }
        }

        // switch places for enabled layers
        let placeHolder = frameEnabledLayers[currentLayer + direction];
        frameEnabledLayers[currentLayer + direction] = frameEnabledLayers[currentLayer];
        frameEnabledLayers[currentLayer] = placeHolder;
        document.getElementById(`layer_enabled_${currentLayer}`).checked = frameEnabledLayers[currentLayer];
        document.getElementById(`layer_enabled_${currentLayer + direction}`).checked = frameEnabledLayers[currentLayer + direction];

        // after changes if we have enabled layer as a disabled
        if (document.getElementById(`layer_enabled_${currentLayer}`).disabled && frameEnabledLayers[currentLayer]){
            document.getElementById(`layer_enabled_${currentLayer}`).disabled = false;
            document.getElementById(`layer_enabled_${currentLayer}`).readOnly = false;
        }

        
        // switch places for names
        placeHolder = frameLayerNames[currentLayer + direction];
        frameLayerNames[currentLayer + direction] = frameLayerNames[currentLayer];
        frameLayerNames[currentLayer] = placeHolder;
        document.getElementById(`layer_name_${currentLayer}`).innerHTML = frameLayerNames[currentLayer];
        document.getElementById(`layer_name_${currentLayer + direction}`).innerHTML = frameLayerNames[currentLayer + direction];


        // switch frame layer list 
        placeHolder = frameLayerList[currentFrame][currentLayer + direction];
        frameLayerList[currentFrame][currentLayer + direction] = JSON.parse(JSON.stringify(frameLayerList[currentFrame][currentLayer]));
        frameLayerList[currentFrame][currentLayer] = JSON.parse(JSON.stringify(placeHolder));

        document.getElementById(`layer_editable_${currentLayer + direction}`).click();
    }


}, false);
