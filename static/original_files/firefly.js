/*
Array.prototype.remove = function() {

    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};
*/

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


//check if DOM is loaded before doing addEventListeners in following code.
window.addEventListener("DOMContentLoaded", function() {
//the whole codeblock is in this eventlistener event which gets execute when the whole DOM is loaded.

MAX_LEDS  = 54;										//max leds at start, this can be changed from the GUI be changing
var currentFrame 	= 0;								//point to the framenumber the user is currently working on.
var currentSpeed 	= 250;								//default playback speed
var bounce			= false;							//play state, should i bounce or repeat on looping
var bounceUp 		= true;
var repeat			= false;							//play state, should i bounce or repeat on looping
var playing			= false;							//is the animation playing in realtime

var frameList;
var animation;
var animatedCurrentFrame = 0;
var currentNodeID = null; // unused?
var nodeList = document.getElementById("tb_lightboxSelect");
nodeList.options[nodeList.options.length] = new Option("No free node!", '-1'); // Initial value

function getLightboxID(){
	return parseInt(nodeList.value);
}

currentSaveFile 	= "";							//holds the filename of the savefile when
selectedDrawTool	= document.getElementById("btn_pencil");	//set the selected tool to pencil at start.
viewmasterStartfrom = 0;							//the frame preview buttons 1-9 at the bottom of the led view.
viewmasterPointer 	= 0;							//holds currently selected viewpoint
writeto 			= false;
colorPointer 		= 3; 							//counter used to cycle through colors wih keyboard.
													//starts at 3 because colorbox3 is the startcolor (red).
/* OK */
function randomId() {								//function used to generate a unique cookie-id.
	const uint32 = window.crypto.getRandomValues(new Uint32Array(1))[0];
	return uint32.toString(16);
}
/* OK */
function getCookie(name) {							//return the value of a stored cookie.
	var dc = document.cookie;
	var prefix = name + "=";
	var begin = dc.indexOf("; " + prefix);
	if (begin == -1) {
		begin = dc.indexOf(prefix);
		if (begin != 0) return null;
	}
	else
	{
		begin += 2;
		var end = document.cookie.indexOf(";", begin);
		if (end == -1) {
		end = dc.length;
		}
	}
	return decodeURI(dc.substring(begin + prefix.length, end));
}

var authToken = randomId();
//document.cookie = 'userId=' + authToken + '; path=/'; //mind the space after ; !
myCookie = getCookie("userId");
console.log(document.cookie);
/* OK */
if (myCookie == null) {											//if cookie does not exist -> we have a new user
	document.cookie = 'userId=' + authToken + '; path=/';		//create cookie with these values.. mind the space after; !
	document.cookie = 'defaultRows=6' + '; path=/';				//doesnt work on all platforms if space is missing
	document.cookie = 'defaultCols=6' + '; path=/';
	document.cookie = 'defaultLeds=54' + '; path=/';
	document.cookie = 'userName=Anonymous' + '; path=/';
	document.cookie = 'path=/' + '; path=/';

	console.log("Created new cookie, userId=" + authToken);
}
else {
	console.log("Cookie found, userId=" + myCookie);			//user already visited and has a cookie
}


function addNodeID(nodeID){
	console.log("node found: " + nodeID);
	for (var i = 0; i < nodeList.length; i++) {
		var option = nodeList.options[i];
		if (option.value == -1) {
			option.text = "Pick a node";
		}else if (option.value == nodeID) {
			return;
		}
	}
	nodeList.add(new Option("Node " + nodeID, nodeID));
	setStatusText("Node " + nodeID + " online", "normal");

}

function removeNodeID(nodeID){
	console.log("node disconnected: " + nodeID);

}

var source = new EventSource('/stream?channel=fromMesh');
source.onmessage = function(e) {
	var data = JSON.parse(e.data);
	if(data.hasOwnProperty('type')){
		switch(data.type){
			case 'connect':
			case 'pong':
				addNodeID(data.node);
				break;
			case 'disconnect':
				removeNodeID(data.node);
				break;

		}
	}






		console.log("[fromMesh] --" + "> " + e.data);
};
source.onopen = function(e) {
		setStatusText("Connected", "normal");
};
source.onerror = function(e) {
		setStatusText("Not connected", "urgent");
};



var mouseDown = false;											//variabble holding current mouseState
document.body.onmousedown = function() 	{ mouseDown = true; }	//save mouseDown status on mouseUp
document.body.onmouseup = function() 	{ mouseDown = false; }	//save mouseDown status on mouseDown

/* REMOVE?
websocket.onmessage = function (event) {						//process incoming websockets messages here
	console.log("Incoming data :" + event.data);
	//example of JSON msg : {"msgtype": "getLightboxList", "dataWritingPipesPool": [2, 3, 4, 5, 6], "dataWritingPipes": [1, 2, 3, 4, 5, 6], "dataSelfPipe": 1}

	var msg = JSON.parse(event.data);
	if(msg.hasOwnProperty("msgtype"))
	{
		if (msg.msgtype=='getFileList'){								//build the loadFile dropdown list from the json data
			buildFileList(msg.data);
		}

		if (msg.msgtype=='getLightboxList'){
			console.log(msg.data)										//build the selectNode dropdown list from the json data
			buildLightboxList(msg.dataWritingPipesPool, msg.dataWritingPipes, msg.dataSelfPipe);
		}

		if (msg.msgtype=='loadFile'){									//what to do when the lbController sends the data of the file we want to load.
			console.log(msg.data);
			frameList = msg.data;										//copy whole animation into frameList variable
			currentFrame = 0;											//set currentFrame pointer to 0, first frame
			console.log(Object.keys(frameList[0]).length);
			MAX_LEDS = Object.keys(frameList[0]).length;				//get dictionary size of the first frame. From this value, you know how many leds there are in the frame.
			dimensionBtn( msg.rows, msg.cols, Object.keys(frameList[0]).length, msg.doLedDirection, false);	//create a new ledDirection array with the new height/rows
			console.log(msg.doLedDirection)
			document.getElementById("ledDirection").checked= msg.doLedDirection;		//if ledDirection as asked, mark is so in the userSettings window.
			//sendMod('gotoFrame', currentFrame)
			viewmasterStartfrom = 0;									//reset viewmaster to first frame
			viewmasterPointer = 0;
			renderViewmaster();											//show correct viewmaster update
			renderLeds();
		}
	}
}
*/
//Build animation matrix of leddata
//Eaxh animation has multiple frames so we stored all frame data in an array (a list becuse the position in the array is important)
//Every frameList item points to a dictionary with all leddata of the frame. This is a dictionary.
//dictionary key value always get converted to string with json.

frameList		= [];  //create array
frameList[0] 	= {}; //create first frame as dictionary

for(var i=0; i < MAX_LEDS; i++){
	frameList[0][i] = "0:0:0"			//fill first frame dictionary with empty RGB values.
}

var copyPaste = {}; //make empty dict frame to use as  a copy/paste buffer
renderViewmaster();
document.getElementById("tb_colorselect").style.backgroundColor="red"

/////////////////////////////////////
// Start of EVENT LISTENERS adding
/////////////////////////////////////
//activate changeCurrentColor on each element of the colorbox class.
var colorbox = document.getElementsByClassName("colorbox");
	for (var i = 0; i < colorbox.length; i++) colorbox[i].addEventListener("click", changeCurrentColor);

//activate changeCurrentColor on each element of the colorbox class.
function attachLedActions(){
	//For every LED box, we add two functions :
	//		onClick -> this function sends changed single LED color data to the lbController.
	//		onHovr  -> this function sends mouse cursor position to the lbController.
	var leds = document.getElementsByClassName("boxed_led");
	for (var i = 0; i < leds.length; i++) leds[i].addEventListener("click", changeLedColor);
	for (var i = 0; i < leds.length; i++) leds[i].addEventListener("mouseover", changeLedColorHover);

	//For every frame goto butyon, we add two functions :
	//		onClick -> Function asks the lbController to show other frame.
	//		onHover  -> Function copies duplicates the previous frame for easy use.
	var previews = document.getElementsByClassName("boxed_preview");
	for (var i = 0; i < previews.length; i++) previews[i].addEventListener("click", previewClick);
	for (var i = 0; i < previews.length; i++) previews[i].addEventListener("dblclick", previewDubbelClick);
}

attachLedActions();		//runs previous function

document.getElementById("speedSliderMembank").addEventListener("input", speedSliderMembankChange);	//Change playback speed of a animation to save.
document.getElementById("speedSlider"		).addEventListener("input", speedSliderChange);		//Change placyback speed of live play
document.getElementById("speedSlider"		).addEventListener("mouseup", speedSliderMouseUp);	//for desktop support
document.getElementById("speedSlider"		).addEventListener("touchend", speedSliderMouseUp);	//for touch based support
document.getElementById("btn_pencil"		).addEventListener("click", selectDrawTool);		//all 3 draw btns points point to same function
document.getElementById("btn_filler"		).addEventListener("click", selectDrawTool);		//
document.getElementById("btn_eyedropper"	).addEventListener("click", selectDrawTool);		//
document.getElementById("btn_clearall"		).addEventListener("click", clearAll);				//clear all pixels function
document.getElementById("tb_colorselect"	).addEventListener("click", colorSelect);			//change color function
document.getElementById("btn_insertframe"	).addEventListener("click", insertFrame);
document.getElementById("btn_deleteframe"	).addEventListener("click", deleteFrame);
document.getElementById("btn_previous"		).addEventListener("click", previous);
document.getElementById("btn_next"			).addEventListener("click", next);
document.getElementById("btn_firstframe"	).addEventListener("click", firstFrame);
document.getElementById("btn_lastframe"		).addEventListener("click", lastFrame);
document.getElementById("btn_copy"			).addEventListener("click", copy);					//copy whole frame to buffer function
document.getElementById("btn_paste"			).addEventListener("click", paste);					//paste whole frame to buffer function
document.getElementById("btn_play"			).addEventListener("click", play);
document.getElementById("btn_stop"			).addEventListener("click", stop);
document.getElementById("btn_repeat"		).addEventListener("click", repeatBtn);				//'repeat' is a reserved word. changed name to repeatBtn instead.
document.getElementById("btn_bounce"		).addEventListener("click", bounceBtn);
document.getElementById("btn_save"			).addEventListener("click", saveFile);
document.getElementById("tb_fileselect"		).addEventListener("change", loadFile);				//if user clicks a file to load
document.getElementById("tb_lightboxSelect"	).addEventListener("change", lightboxSelect);		//user selects a new lightbox from dropdown
document.getElementById("changeMatrixBtn"	).addEventListener("click", changeMatrix);			//user changes row/col dimension in userSettings menu
document.getElementById("saveDefaultsBtn"	).addEventListener("click", saveDefaults);			//user saves default userSettings parameters and save it in cookie
document.getElementById("memSaveBtn").addEventListener("click", MemorySave);		//Exec MemorySave() when clicked on MemorySave button

document.getElementById("btn_pencil").click();			//set pencil as initial selected tool.
repeatBtn(); 											//set repeat button as initial state of repeat button.

//Show the popup message with keyboard shortcuts you can use.
document.getElementById("btn_help").addEventListener("click", function() {
	helptext ="T = select tool\nP = play\nS = stop\nC = copy frame\nD = Download animation\nV = paste frame\nE = eraser all\nB = bounce\nR = repeat\nleft = previous frame\nright = next frame\nUp = Colorcyle up\nDown = Colorcyle down\ninsert = insert frame\ndel = delete frame\nhome/pageup = first frame\nend/pagedown = last frame\n";
	alert(helptext);
});

//variables pointing to various elements for easy access.
var btn_memsave 	 = document.getElementById("btn_memsave");
var btn_settings 	 = document.getElementById("btn_settings");
var membankModal 	 = document.getElementById("membankModal");
var settingsModal 	 = document.getElementById("settingsModal");
var membankCloseBtn	 = document.getElementById("membankCloseBtn"); 		// Get the <span> element that closes the modal window
var settingsCloseBtn = document.getElementById("settingsCloseBtn"); 	// Get the <span> element that closes the modal window

/* OK */
// When the user clicks the save to memory-bank-button, open the modal window.
btn_memsave.onclick = function() {
	membankModal.style.display = "block";
}

/* OK */
btn_settings.onclick = function() {										//Function which sets default values in userSetting menu
	document.getElementById("settingsErrorLabel").innerHTML="";

	newName = document.getElementById("defaultUsername");
	newRows = document.getElementById("defaultRows");
	newCols = document.getElementById("defaultCols");
	newLeds = document.getElementById("defaultLeds");

	newName.value = getCookie("userName");
	newRows.value = getCookie("defaultRows");
	newCols.value = getCookie("defaultCols");
	newLeds.value = getCookie("defaultLeds");

	settingsModal.style.display = "block";								//show window as modal
}
/* OK */
membankCloseBtn.onclick = function() {									// Close window when clicking on the X of the membank save window
	membankModal.style.display = "none";
}
/* OK */
settingsCloseBtn.onclick = function() {									// Close window when clicking on the X of the membank save window
	settingsModal.style.display = "none";
}
/* OK */
// When the user clicks anywhere outside of the membank save window of user settings window, close it.
window.onclick = function(event) {
	if (event.target == membankModal || event.target == settingsModal) {
		membankModal.style.display	= "none";
		settingsModal.style.display	= "none";
	}
}
/* OK */
function addCss(rule) {
	let css = document.createElement('style');
	css.type = 'text/css';
	if (css.styleSheet) css.styleSheet.cssText = rule;					// Support for IE
	else css.appendChild(document.createTextNode(rule));				// Support for the rest
	document.getElementsByTagName("head")[0].appendChild(css);			//add css rule to the HEAD of the document
}
/* OK */
function setStatusText(message, priority){								//shows a sliding popup from the top with custom message
	console.log("Popup:" + message);
	note = document.getElementById("note");
	if (priority=="normal") {note.style.backgroundColor ="fde073";}		//use different color depending on msg type
	if (priority=="urgent") {note.style.backgroundColor ="ff9933";}
	note.innerHTML = message;
	note.classList.remove("noteanimation");								//first remove animation
	note.offsetWidth; 													//https://css-tricks.com/restart-css-animation/
	note.classList.add("noteanimation");								//then add animation to activate it
}
/* OK */
function selectDrawTool(event){											//Visualise the selected tool by inverting its background color.
	selectedDrawTool.style.filter='none'								//reset style of current selectedDrawTool
	this.style.backgroundColor ="white";
	this.style.filter="invert(100%)";
	selectedDrawTool = this;											//remember which tool was selected to use in other functions
}


/* NOK: dependancy sendFrame */
function clearAll(){													//change all LEDS to black and send frame update to lbController.
	leds = document.getElementsByClassName("boxed_led");
	for(var i=0; i< leds.length; i++) {
		leds[i].classList.add("ledview_bg");
		leds[i].style.backgroundColor = "#fff";
		frameList[currentFrame][i] = "0:0:0";
	}
	sendFrame(frameList)
}
/* OK */
//Function to change the color to use.
function changeCurrentColor(event){
	if (event.target.id == "tb_color_11"){								//In case of drawing 'nothing' aka transparant.
		document.getElementById("tb_colorselect").classList.add("transparantBg");
		//remove the elements which are added by the color pick plugin at start.
		document.getElementById("tb_colorselect").style.removeProperty("background-image");
		document.getElementById("tb_colorselect").style.removeProperty("background-color");
		document.getElementById("tb_colorselect").style.removeProperty("color");
	}
	else{
		document.getElementById("tb_colorselect").classList.remove("transparantBg");
		document.getElementById("tb_colorselect").style.backgroundColor = getComputedStyle(document.getElementById(this.id)).backgroundColor;
	}
}
/* OK */
//If user clicks the big selected-color box, change current drawing tool to pen.
function colorSelect(){
	document.getElementById("btn_pencil").click();
}

/* UPDATE */
//function used to send which LED field the mouse if hoverring over. A blinking cursor is displayed on the lightbox.
function changeLedColorHover(event){
	if(mouseDown==true){
		this.click(); 									//makes you 'click and slide' to keep drawing
		return;											//return so it does not do a blinker to so overload.
	}


	if (getLightboxID() == -1 || document.getElementById('sendBlinker').checked == false) return; 									//dont send blinker data when no lightbox is selected.


	selectedLed = parseInt(this.dataset.nr)-1;							//leds numbering start at 1 html layout, but 0 in Lbcontroller.

	// UPDATE: to new post
	toMesh({"type":"PlayAnimation",  "id": getLightboxID(),  "memorybank":253, "speed":selectedLed});

}

//Function
function changeLedColor(event){
	selectedLed = parseInt(this.dataset.nr)-1;

	//when using pencil tool..
	if(selectedDrawTool==document.getElementById("btn_pencil")){							//in case pencil + transparant color is selected
		if(document.getElementById("tb_colorselect").classList.contains("transparantBg")){
			this.style.backgroundColor = "#fff";											//add white background
			this.classList.add("ledview_bg");												//add the stripes on top of the white background
			frameList[currentFrame][selectedLed] = "0:0:0";									//set to black (=transparant)
		}
		else {																				//in case pencil + a color is selected
			this.style.backgroundColor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor;
			this.classList.remove("ledview_bg");											//remove the stripes background
			var rgbcolor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor;
			var rgb = rgbcolor.match(/\d+/g);	//extract color information with regex from previous getComputedStyle string
			red = rgb[0];
			green = rgb[1];
			blue = rgb[2];
			frameList[currentFrame][selectedLed] = rgb[0] + ":" + rgb[1] + ":" + rgb[2];	//update internal LED data with color update
		}
		sendSingleLedJSON(selectedLed, frameList[currentFrame][selectedLed]);				//send led color update
	}

	//when using filler tool..
	if(selectedDrawTool==document.getElementById("btn_filler")){							//select color fill tool

		if(document.getElementById("tb_colorselect").classList.contains("transparantBg")){	//if transparant color selected ?
			let allLeds = document.getElementsByClassName("boxed_led");
			for (var i = 0; i < allLeds.length; i++) {
				allLeds[i].style.backgroundColor = "#fff";									//set background to white
				allLeds[i].classList.add("ledview_bg");										//add the stripes that white background
				frameList[currentFrame][i] = "0:0:0";
			}
		}
		else {																				//if NON transparant color selected ?
			let allLeds = document.getElementsByClassName("boxed_led");
			var rgbcolor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor;
			var rgb = rgbcolor.match(/\d+/g);
			red = rgb[0];
			green = rgb[1];
			blue = rgb[2];

			for (var i = 0; i < allLeds.length; i++) {
				allLeds[i].style.backgroundColor = getComputedStyle(document.getElementById("tb_colorselect")).backgroundColor;
				allLeds[i].classList.remove("ledview_bg");									//remove the stripes background

				frameList[currentFrame][i] = rgb[0] + ":" + rgb[1] + ":" + rgb[2];
			}
		}
		sendFrame(frameList);																//Send ALL leds because they have all changed.
	}

	//when using eyedropper tool
	if(selectedDrawTool==document.getElementById("btn_eyedropper")){
		document.getElementById("tb_colorselect").style.backgroundColor = getComputedStyle(document.getElementById(this.id)).backgroundColor;
		document.getElementById("btn_pencil").click();
	}

}

//Function to redraw all led fields with its color value.
function renderLeds(){
	document.getElementById("tb_currentframe_text").innerHTML = (currentFrame+1) + "/" + frameList.length;	//update gui element
	for(i=0; i<MAX_LEDS; i++){																		//loop over all leds of current frame
		ledtodo = "ledview_" + (i+1).toString()
		rgb = frameList[currentFrame][i]
		rgbSplit = rgb.split(":")
		rgbString = "rgb(" + rgbSplit[0] + "," + rgbSplit[1] + "," + rgbSplit[2]+ ")";				//extract rgb data from each led

		if (rgbSplit[0]=="0" && rgbSplit[1]=="0" && rgbSplit[2]=="0") {								//0:0:0 means all black
			document.getElementById(ledtodo).classList.add("ledview_bg");							//add the stripes background
			document.getElementById(ledtodo).style.backgroundColor = "white";
		}
		else{
		document.getElementById(ledtodo).style.backgroundColor = rgbString;							// it has a color other than full black
		document.getElementById(ledtodo).classList.remove("ledview_bg");							//remove the stripes background
		}
	}
}

//Add new frame in frameList and assign new dictionary entry to it. Fill it with led values after.
function insertFrame(){
	frameList.splice( currentFrame+1, 0, frameList[currentFrame+1]);
	frameList[currentFrame+1]={}
	for(var i=0; i < MAX_LEDS; i++){ //add led with rgb value for each
		frameList[currentFrame+1][i] = "0:0:0"
	}
	currentFrame += 1;
	sendFrame(frameList, currentFrame);
	updateViewmaster('up')
	renderViewmaster();
	renderLeds();
}

//Deleted a frame and redraws the leds and viewmaster buttons.
function deleteFrame(){
	if (frameList.length> 1){
		if(currentFrame==(frameList.length)-1){
			console.log("last frame detected")
			frameList.splice(currentFrame,1);
			currentFrame = frameList.length-1;
			updateViewmaster('down')

		}
		 else if(currentFrame==0){
			console.log("first frame detected")
			frameList.splice(currentFrame,1);
			currentFrame = 0;
		}
		else {
			frameList.splice(currentFrame,1);
		}
		sendFrame(frameList, currentFrame);
		renderViewmaster();
		renderLeds();
	}
}

//function keeps tracks where the highlighted previewbox is pointing to
//so its scrolls correctly when using previous and next buttons.
function updateViewmaster(updown){

	if (updown=='up') {
		if(viewmasterPointer==8) viewmasterStartfrom++;
		if(viewmasterPointer<8) viewmasterPointer++;
	}
	if (updown=='down') {
		if(viewmasterPointer==0) viewmasterStartfrom--;
		if(viewmasterPointer>0) viewmasterPointer--;
	}

	if(viewmasterStartfrom<0) viewmasterStartfrom=0;
	console.log(updown + " Pointer:" + viewmasterPointer.toString() + " Startfrom:" +  viewmasterStartfrom.toString())
	renderViewmaster()
}

//Function to draw the preview buttons at the bottom.
function renderViewmaster(){
	console.log("Rendering preview boxes.")
	document.getElementById("tb_currentframe_text").innerHTML = (currentFrame+1) + "/" + frameList.length;
	for(i=0;i<9;i++){ //clear all preview boxes
		preview = "preview_" + (i+1).toString();
		document.getElementById(preview).innerHTML = "";
		document.getElementById(preview).style.backgroundColor = "white";
		document.getElementById(preview).style.cursor = "pointer";
	}

	framecount = frameList.length;
	if (framecount>9) framecount=9;
	if (frameList.length>9) startfrom = currentFrame;

	for(i=0; i<9; i++){
		preview = "preview_" + (i+1).toString();
		document.getElementById(preview).style.cursor = "pointer";
		if (i< framecount) {
			document.getElementById(preview).innerHTML = viewmasterStartfrom+i+1;
		}else {
			document.getElementById(preview).innerHTML = "&nbsp;";
			document.getElementById(preview).style.cursor = "default";
		}
		document.getElementById(preview).style.backgroundColor = "black";
		document.getElementById(preview).style.color="white"

		if (i==viewmasterPointer){
			document.getElementById(preview).style.backgroundColor = "black";
			document.getElementById(preview).style.color="red";
			}
	}
}
//jump to next frame and adjust gui
function next(){
	if (currentFrame< frameList.length -1){
		currentFrame += 1;
		renderLeds();
		sendFrame(frameList, currentFrame);
		updateViewmaster('up')
	}
}

//jump to previous and adjust gui
function previous(){
	console.log(currentFrame);
	if (currentFrame>0){
		currentFrame -= 1;
		renderLeds();
		sendFrame(frameList, currentFrame);
		updateViewmaster('down')
	}
}

//jump to firtframe and adjust gui
function firstFrame(){
	currentFrame=0;
	viewmasterPointer=0;
	viewmasterStartfrom=0;
	renderViewmaster();
	renderLeds();
	sendFrame(frameList, currentFrame);
}

//jump to lastframe and adjust gui
function lastFrame(){
	currentFrame=frameList.length - 1;
	if (frameList.length>9){
		viewmasterPointer=8;
		viewmasterStartfrom = frameList.length - 9;
	}
	else{
		viewmasterPointer= frameList.length-1;
		viewmasterStartfrom=0;
	}
	renderViewmaster();
	renderLeds();
	sendFrame(frameList, currentFrame);
}

//If you click a preview box, you jump to the right frame
function previewClick(){
	clickedNr = parseInt(this.dataset.nr);
	if (clickedNr>frameList.length - 1) return;			//skip if clicking on empty preview div.
	currentFrame=(viewmasterStartfrom+clickedNr);
	sendFrame(frameList, currentFrame);
	viewmasterPointer = clickedNr;
	renderViewmaster();
	renderLeds();
}

//the last frame number of the preview buttons is clickable.
//if you click it, you will duplicate current frame to the newly next one.
function previewDubbelClick(){
	console.log("nen dubbel");
	clickedNr = parseInt(this.dataset.nr);

	if (clickedNr+viewmasterStartfrom==frameList.length-1){ 	//exec this function only on the first empty preview button.
		console.log("execute double");
		currentFrame = frameList.length-1; 						//set currentframe to last frame.
		quickcopy = Object.assign({}, frameList[currentFrame]) 	//copy contents of last frame
		insertFrame();											//insert empty frame afger last frame
		frameList[currentFrame] = Object.assign({}, quickcopy); //currentFrame is now to +1, restore contents from copy
		sendFrame(frameList);
		renderViewmaster();
		renderLeds();
	}
}

//you can copy a whole frame
function copy(){
		copyPaste = Object.assign({}, frameList[currentFrame])
}
//and paste it on another frame
function paste(){
		frameList[currentFrame] = Object.assign({}, copyPaste);
		sendFrame(frameList);
		renderLeds();
}

//stop the animation played by current user
function stop(){
	console.log("stopping animation");

	playing= false;
	playBtn= document.getElementById("btn_play");
	playBtn.style.filter="invert(0%)";
	playBtn.style.backgroundColor ="white";

	clearInterval(animation);
}

function player(){
	if(getLightboxID() == -1) return;

	if(animatedCurrentFrame == frameList.length){
		if (repeat == false && bounce == false){
			clearInterval(animation);
			return;
		}
		if(bounce){
			animatedCurrentFrame -= 2;
			bounceUp = false;
		}else{
			animatedCurrentFrame = 0;
			bounceUp = true;
		}
	}else if(animatedCurrentFrame == -1){
				animatedCurrentFrame = 0;
				bounceUp = true;
	}
	sendFrame(frameList, bounceUp?animatedCurrentFrame++:animatedCurrentFrame--);
//animatedCurrentFrame

	var flow='once';
	if (repeat) flow='repeat';
	if (bounce) flow='bounce';
	console.log({action: 'play', lightboxNr: getLightboxID(), flow: flow, speed: currentSpeed});
}

//Start animation play and visualise it by toggling play button
function play(){
	animatedCurrentFrame = 0;
	console.log("playing animation");
	playing=!playing;									//variable keeps state if you click it or not
	playBtn = document.getElementById("btn_play");
	if (playing){
		playBtn.style.filter="invert(100%)";
		playBtn.style.backgroundColor ="white";
	}
	else {
		playBtn.style.filter="invert(0%)";
		playBtn.style.backgroundColor ="white";
		stop();
		return;
	}

	animation = setInterval(player, currentSpeed);


}

//function enables bounce mode and visualise it by inverting colors.
function bounceBtn(){
	bounce=!bounce;
	btn_bounce = document.getElementById("btn_bounce");
	btn_repeat = document.getElementById("btn_repeat");

	if (bounce){
		btn_bounce.style.filter="invert(100%)";
		btn_bounce.style.backgroundColor ="white";
		btn_repeat.style.filter="invert(0%)";
		btn_repeat.style.backgroundColor ="white";
	}
	else {
		btn_bounce.style.filter="invert(0%)";
		btn_bounce.style.backgroundColor ="white";
	}
	repeat=false;
}

//function enables repeat mode and visualise it by inverting colors.
function repeatBtn(){
	repeat=!repeat;
	btn_bounce = document.getElementById("btn_bounce");
	btn_repeat = document.getElementById("btn_repeat");

	if (repeat){
		btn_repeat.style.filter="invert(100%)";
		btn_repeat.style.backgroundColor ="white";
		btn_bounce.style.filter="invert(0%)";
		btn_bounce.style.backgroundColor ="white";
	}
	else {
		btn_repeat.style.filter="invert(0%)";
		btn_repeat.style.backgroundColor ="white";
	}
	bounce=false;
}
/*
//this function ask for modifications in the animation (assing frames, deleting frames)
function sendMod(_action, _nr){
	sendFrame(frameList, _nr);
	//toMesh({action: _action, framenr: _nr});
}
*/
//send update color of a single led
function sendFrame(_frameDict, frameToSend = currentFrame){
	var pixels = [];
	var start = parseInt(Object.keys(_frameDict[frameToSend])[0]);
	for (var [ledNr, rgb] of Object.entries(_frameDict[frameToSend])) {
		ledNr = parseInt(ledNr);
		rgb = rgb.split(":").map(Number)
		pixels.push(rgb);

	}
	if(isNaN(start) || pixels.length === 0) return;
	toMesh({"type":"SetLED", "id": getLightboxID(), "start": start, "pixels":pixels});
}
//send update colors of a whole frame
function sendSingleLedJSON(ledNr, leds){
	toMesh({"type":"SetLED", "id": getLightboxID(),"start": ledNr, "pixels":[leds.split(":").map(Number)]});
}

function getFileList(){
	toMesh({action: 'getFileList'});
}

function buildFileList(msgdata){
	console.log("- Building filelist dropdown.");
	msgdata.sort();														//sort the filelist first
	var fileBox = document.getElementById("tb_fileselect");

	fileBox.options.length = 1;	//reset whole list minus 1, leaving only the first option with the text('load animation') available.
	for (var key in msgdata) {	//build option list
		fileBox.options[fileBox.options.length] = new Option(msgdata[key], msgdata[key]);
	}

	for (var i = 0; i < fileBox.options.length; i++) {				//set the default picked option to the chosen savefile
		if (fileBox.options[i].text === currentSaveFile) {
			fileBox.selectedIndex = i;
			break;
		}
	}
}

//Function which build a whole dropdown list by a given list of items.
//writingPipesPool ;  list of all connected lightboxes (taken or not))
//Writing pipes = list of all connected lightboxes in use (selected by the user)
//selfPipe = the lightbox assigned to you

function buildLightboxList(writingPipesPool, writingPipes, selfPipe){
	console.log("- Building nodelist dropdown.");

	var lbSelector = document.getElementById("tb_lightboxSelect");
	lbSelector.options.length = 0; //reset list
	lbSelector.disabled = false;
	if (selfPipe===null) {																		//if you dont have been assigned a lightbox yet
		if (writingPipesPool.length > 0) {														//and new one are available
			setStatusText("A node got available again!","normal");								//show popup msg to inform the user
			lbSelector.options[lbSelector.options.length] = new Option("Pick a node", '-1');
			lbSelector.options[0].disabled = true;												//make the 'pick a node' option non clickable
		}
		else {
			setStatusText("No free nodes available!","urgent");
			lbSelector.options[lbSelector.options.length] = new Option("No free node!", '-1');	//Show msg in dropdown that no nodes are available.
			lbSelector.options[0].disabled = true;												//make the 'pick a node' option non clickable
		}
	}

	//loop over nodeList and rebuild the dropdown box in html code.
	for (var key in writingPipes) {
		optionString = "Node " + writingPipes[key].toString();

		lbSelector.options[lbSelector.options.length] = new Option(optionString, writingPipes[key]);
		//if not nodes available, disable the dropdown box
		if (writingPipesPool.indexOf(writingPipes[key]) == -1) lbSelector.options[lbSelector.options.length-1].disabled = true;
		//grayout your currently selected lightbox
		if (writingPipes[key] == selfPipe) lbSelector.options[lbSelector.options.length-1].disabled = false;
		//Set the default selected option to your own lightbox number
		if (writingPipes[key] == selfPipe) {
			lbSelector.options[lbSelector.options.length-1].selected = true;
		}
	}
}

//Function asks lbController to load an animation file and pass animation data to the browser.
function loadFile(){
	if (this.selectedIndex==0) return;
	console.log('LOADING');
	currentSaveFile = this.options[this.selectedIndex].text;
	toMesh({action: 'loadFile', fileName: this.options[this.selectedIndex].text });
}

//Function asks lbController to assign a lightbox to current user
function lightboxSelect(){
  document.getElementById("node").value = getLightboxID();
	//toMesh({action: 'lightboxSelect', lightboxNr: getLightboxID()});

}

//Function to change the led grid when using non standard 9*5 lightbox.
//The hole ledfield gets rebuild, incuding the ledDirection array.
//The function build the same html strucuture as before with other domensions.
//Only used when the user changes the layout or request a layout because its
//saved in the cookie as default.
function dimensionBtn(rows, cols, numLeds, ledDirection, startEmpty){
	div = document.getElementById("ledview");

	while(div.firstChild){
		div.removeChild(div.firstChild);						//remove whole led grid.
	}
	MAX_LEDS = numLeds;
	dataNr = 1;													//lednumbering in html code starts from 1
	htmlString = "";											//will hold the full html string describing the new led grid.

	//toprow with numbering
	htmlString += '<div id="ledview_fly" 	class="box ledview_toprow"><img class="fflogoimg_inv" src="./img/fireflylogo_inv.png"></div>';
	for (c=1; c<=cols; c++){
		htmlString += `<div id="ledtop_${c}" class="box ledview_toprow">${c}</div>`;
	}

	//build the led grid view
	for (r=1; r<=rows; r++){
		htmlString += `<div class="box ledview_leftcol">A${r}</div>`;
		for (c=1; c<=cols; c++){
			if (dataNr <=numLeds)
				{
				htmlString += `<div id="ledview_${dataNr}" class="ledview_bg box boxed_led" data-nr="${dataNr}"></div>`;
				}
			else
				{
				htmlString += `<div class="box"></div>`;		// if ledCount < rows * cols : add empty box to make grid fit
				}
			dataNr += 1;
		}
	}

	//previewrow
	htmlString += '<div id="preview_P" 	class="box ledview_leftcol"></div>';
	htmlString += '<div id="preview_all">';
	for (c=1; c<=9; c++){
		htmlString += `<div style="width:90px; float:left; margin: 0px 5px 0px 0px;" id="preview_${c}" class="box boxed_preview" data-nr="${c-1}">&nbsp;</div>`;
	}
	htmlString += '</div>';
	div.style.gridTemplateColumns =  `40px repeat(${cols}, 1fr)`;
	div.style.gridTemplateRows =  `40px repeat(${rows}, 1fr) 30px`;

	div.innerHTML += htmlString;
	//reinitialise framelist
	if(startEmpty){					//start with an empty frameList or keep the existing one
		frameList = [];
			frameList[0]={}; 		//create first frame as dictionary
			for(var i=0; i < MAX_LEDS; i++){
				frameList[0][i] = "0:0:0"
			}
		toMesh({action: 'newdimension', numLeds: numLeds, rows: rows, cols: cols, ledDirection: ledDirection});
	}

	firstFrame();			//goto first frame and display it
	attachLedActions();		//you must reattach the functions after rebuilding al LED grid boxes.
}

function saveFile(){

  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  var time = today.getHours() + "h" + today.getMinutes() + "m" + today.getSeconds() + "s ";
  var dateTime = date+' '+time;
  if(getLightboxID() != -1) dateTime += "Node " + getLightboxID();


  var filename = prompt("Filename?", "animation " + dateTime);
  if (filename == null) {
    return;
  }

	var dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(frameList));

	var dlAnchorElem = document.getElementById('download');
	dlAnchorElem.setAttribute("href",     dataStr     );
	dlAnchorElem.setAttribute("download", filename.endsWith(".json")?filename : filename + ".json");
	dlAnchorElem.click();

  return false;

	/*
	console.log('SAVING');
	select = document.getElementById("tb_fileselect");
	var fileName = prompt("Please enter filename (a-z,0-9 characters)", currentSaveFile);
	if (fileName=='') {
		setStatusText("No filename given! file not saved","urgent")
		return;
	}
	if (!fileName) {
		setStatusText("User cancelled, file not saved.","urgent")
		return;
	}
	if (! /^[a-zA-Z0-9_-]*$/i.test(fileName) && fileName.length<50) {	//filter weird characters and limit string length
		setStatusText("Invalid name! File not saved.","urgent")
		return;
	}

	currentSaveFile = fileName
	toMesh({action: 'saveFile', fileName: fileName });
	//update filelist with newly saved files if chosen a new filename
	getFileList()
	setStatusText("File saved","normal");
	*/
}

async function MemorySave(){
	//https://codepen.io/Hebe/details/KddqNa/ for button progress bar
	stop(); 																//stop current play of the animation to stop ongoing websocket traffic.
	console.log('SAVING to memory');

	autoplay 	= (document.querySelector('input[name="autoplay"]:checked').value=="yes")?1:0;
	playback 	= (document.querySelector('input[name="playback"]:checked').value=="loop")?1:0;		//sends the literal text, not 1 or 0
	bank 	 		= parseInt(document.querySelector('input[name="bank"]:checked').value);
	speed	 		= parseInt(document.getElementById("speedSliderMembank").value);






	var pixels = [];
	var start = 0;

	for (const frame of frameList) {
		for (var [ledNr, rgb] of Object.entries(frame)) {
			ledNr = parseInt(ledNr);
			rgb = rgb.split(":").map(Number)
			pixels.push(rgb);
		}
	}

	if(isNaN(start) || pixels.length === 0) return;

	document.getElementById("memSaveBtn").value="Saving";
	document.getElementById("memSaveBtn").disabled=true;
	/*
	background-image: repeating-linear-gradient(-45deg, transparent, transparent 20px, rgba(0, 200, 0, 0.5) 20px, rgba(0, 200, 0, 0.5) 40px);
background-repeat: no-repeat;
  background-size: 20% 100%;
	*/

	var index = 0;
	while (index < pixels.length) {
		var pixelChunk = pixels.slice(index, 8 + index);


		toMesh({"type":"SaveAnimation", "id": getLightboxID(), "start": index, "memorybank":bank, "pixels":pixelChunk});
		index += 8;
		await sleep(2000);
	}


	//toMesh({"type":"SaveAnimation", "id": getLightboxID(), "start": start, "memorybank":bank, "pixels":pixels});


	toMesh({"type":"ConfigureAnimation", "id": getLightboxID(), "memorybank":bank, "autoplay":autoplay, "ledsPerFrame":MAX_LEDS, "frameCount":frameList.length, "speed":speed, "flow":playback, "repeat":0});

	document.getElementById("memSaveBtn").value="Save";
	document.getElementById("memSaveBtn").disabled=false;

/*
	duration = frameList.length * 1600; //1600 is time per frame needed approx to save a frame. Change to make it more accurate.
	let rule = `.is-loading {  animation: progressFill ${duration}ms -0.1s ease-in-out forwards;}`;
	addCss(rule);	//you always have to add the rule to be abe to reactive it again.
	console.log(rule);
	document.getElementById("memSaveBtn").disabled=true;					//disable click during saving
	document.getElementById("memSaveBtn").classList.add("is-loading");		//activate animation
	document.getElementById("memSaveBtn").value="Saving";

	//This timer gets execute after precalculated time to remove the animation and reset the text.
	setTimeout(function(){
		document.getElementById("memSaveBtn").classList.remove("is-loading");
		document.getElementById("memSaveBtn").value="Save";
		document.getElementById("memSaveBtn").disabled=false;
	}, duration+500);
	*/
}

/* OK */
//function update the gui text with the interval in ms.
function speedSliderMembankChange(){
	console.log("memspeed change");
	const scale = (num, in_min, in_max, out_min, out_max) => {
		return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
	num = document.getElementById("speedSliderMembank").value;
	document.getElementById("sliderValueMembank").innerHTML= Math.round(scale(num, 0, 255, 30, 2000))+" ms";
}

/* OK */
//Change to speed of the currently playing animation
//Function executed when slinding the speed slider and change the gui elements together.
//The speedSliderMouseUp() is triggered when letting go the mousebutton and it that function which send the actual speed to lbController.
//This function only does the gui elements, otherwise a speed change is send on every little move.
function speedSliderChange(){
	const scale = (num, in_min, in_max, out_min, out_max) => {
		return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
	num = document.getElementById("speedSlider").value;
	scaledValue = Math.round(scale(num, 0, 255, 250, 2000));			//speed is send as scaled value 0-255
	currentSpeed = scaledValue;
	document.getElementById("speedSliderMembank").value = num; 		//duplicate speed to membankspeed slider
	document.getElementById("sliderValueMembank").innerHTML= scaledValue +" ms";
	document.getElementById("tb_framespeed_title").innerHTML= "SPEED:"+ scaledValue +"ms";
}


//only gets called when release mouse so you only send speed update once.
function speedSliderMouseUp(){
	if(playing){
		clearInterval(animation);
		animation = setInterval(player, currentSpeed);
	}
	/*
	console.log("sending speed update");

	const scale = (num, in_min, in_max, out_min, out_max) => {
		return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}

	num = document.getElementById("speedSlider").value;
	scaledValue = Math.round(scale(num, 0, 255, 200, 2000));		//calculated on scale of 2000 ms
	lightbox = document.getElementById("tb_lightboxSelect");

	toMesh({action: 'speed', lightboxNr: getLightboxID(), speedValue : scaledValue});
	*/
}

/* OK */
//function to change the # of rows and cols of the animation (if using different shaped leds boxes)
function changeMatrix(){
	console.log('change matrix');
	newRows = document.getElementById("defaultRows");
	newCols = document.getElementById("defaultCols");
	newLeds = document.getElementById("defaultLeds");
	newLedDirection = document.getElementById("ledDirection");		//is the ledstrip continuous or not

	//input validation
	if (newName.value=="" || newRows.value=="" || newCols.value=="" || newLeds.value=="") {
		document.getElementById("settingsErrorLabel").innerHTML="Fields cannot be empty!";
		return;
	}
	//input validation
	if (newLeds.value > newRows.value * newCols.value) {
		document.getElementById("settingsErrorLabel").innerHTML="Amount of leds has to belower than rows * cols!";
		return;
	}
	//ask lbController to create new animation
	dimensionBtn( newRows.value, newCols.value, newLeds.value, newLedDirection.checked, true);
	//hide window
	settingsModal.style.display = "none";
}

/* OK */
function saveDefaults(){	//this function stores the default led animation settings in a cookie.
	newName = document.getElementById("defaultUsername");		//default name of connection user.
	newRows = document.getElementById("defaultRows");			//default number of rows of new animation
	newCols = document.getElementById("defaultCols");			//default number of cols of new animation
	newLeds = document.getElementById("defaultLeds");			//default number of LEDS

	//input validation
	if (newName.value=="" || newRows.value=="" || newCols.value=="" || newLeds.value=="") {
		document.getElementById("settingsErrorLabel").innerHTML="Fields cannot be empty!";
		return;
	}

	//set cookie values
	document.cookie = 'userName='    + newName.value + '; path=/';	//mind the space between; and path. This is mandatory for some browsers .
	document.cookie = 'defaultRows=' + newRows.value + '; path=/';
	document.cookie = 'defaultCols=' + newCols.value + '; path=/';
	document.cookie = 'defaultLeds=' + newLeds.value + '; path=/';
	console.log('Saved new defaults in cookie.') + '; path=/';
	settingsModal.style.display = "none";							//hide window
}

/* OK */
//all keybord shortcut actions are defined here.
document.addEventListener('keydown', function(event) {

	switch(event.keyCode) {
		case 37:			//left
			previous();
			break;
		case 39:			//right
			next();
			break;
		case 80:			//p
			play();
			break;
		case 83:
			stop();			//s
			break;
		case 45:
			insertFrame(); 	//insert
			break;
		case 46:
			deleteFrame();	//delete
			break;
		case 67:
			copy();
			break;			//c
		case 86:
			paste();  		//v
			break;
		case 36:
			firstFrame(); 	//home
			break;
		case 33:
			firstFrame();  	//end
			break;
		case 34:
			lastFrame();  	//pageup
			break;
		case 35:
			lastFrame(); 	//pagedown
			break;
		case 82:
			repeatBtn(); 	//r
			break;
		case 66:
			bounceBtn(); 	//b
			break;
		case 35:
			lastFrame(); 	//pagedown
			break;
		case 84:			//t - cycle through available tools
			if (selectedDrawTool==document.getElementById("btn_pencil")) {
					document.getElementById("btn_filler").click(); break;
				};
			if (selectedDrawTool==document.getElementById("btn_filler")) {
					document.getElementById("btn_eyedropper").click(); break;
				}
			if (selectedDrawTool==document.getElementById("btn_eyedropper")) {
					document.getElementById("btn_pencil").click(); break;
				}
			break;
		case 40:			//down
			colorPointer += 1;
			if (colorPointer > 11) colorPointer= 1;
			document.getElementById("tb_color_" + colorPointer).click();
			break;
		case 38:			//up
			colorPointer -= 1;
			if (colorPointer < 1) colorPointer= 11;
			document.getElementById("tb_color_" + colorPointer).click();
			break;
		case 69:			//e eraserall
			clearAll();
			break;
		case 68:			//future use. download saved json file to your pc.
			console.log("downloading animation");
			break;

		default:
			//console.log("unknown keypress.") document.getElementById("btn_pencil").click();
		}
});


document.getElementById('commands').addEventListener("submit", function(e) {
  e.preventDefault();
  var data = {};
  var formData = new FormData(document.getElementById('commands'));
	debugger;
  for(var pair of formData.entries()) {
    data[pair[0]] = isNaN(pair[1])?pair[1]:parseInt(pair[1]);
  }

  data["ledsPerFrame"] = MAX_LEDS;
  data["frameCount"] = frameList.length;

toMesh(data);
  //var myJSON = JSON.stringify(data);
  //console.log(myJSON);

});


document.getElementById('files').addEventListener("submit", function(e) {
  e.preventDefault();

  var importedFile = document.getElementById('import-file').files[0];

  var reader = new FileReader();
  reader.onload = function() {
    var fileContent = JSON.parse(reader.result);
    frameList = fileContent;
    renderViewmaster();
    renderLeds();
  };
  reader.readAsText(importedFile);


});



function toMesh(_data){
	if(_data.hasOwnProperty("id") && _data.id == -1) return;//Don't msg to unexisting nodes
	var data = new URLSearchParams();
	_data = JSON.stringify(_data);
	data.append("json", _data);
	fetch('/post', { method: 'post', body: data });
	console.log("[toMesh] <"+"-- " + _data);
}

function checkFileExist(urlToFile) {
        let xhr = new XMLHttpRequest();
        xhr.open('HEAD', urlToFile, false);
        xhr.send();

        return xhr.status != "404";
}

function checkWebSerialApi() {
	// We should define correct port for production project
	let webSerialApiConfigFile = "./webSerialApi.json";

	// It means that we are running webserver
	if (checkFileExist(webSerialApiConfigFile)) {
		// If User is not running the chrome
		if (navigator.userAgent.indexOf("Chrome") === -1) alert("please use the chrome in order to use Serials");
		else {
			// If The Web Serial API is supported.
			if ("serial" in navigator) {
			    // TODO
				// We are gonna finish it whenever we will be having the testing environment


				/*// Prompt user to select any serial port.
				const port = await navigator.serial.requestPort();

				// Get all serial ports the user has previously granted the website access to.
                const ports = await navigator.serial.getPorts();*/


			} else {
				// If the browses is chrome, Web Serial API is supported by chrome but we are still checking for support
				alert('Could not Load Web Serial API');
				console.log('Could not Load Web Serial API');
			}
		}
	} else {
        // We are running on raspberry
        // TODO
        null;
	}
}
}, false);
