# Urban Species Firefly

## Description
Web based LED matrix animation software an interface to the Urban Species IB node software. 
http://www.urbanspecies.org

Documentation under construction.

## Installation
Point Google Chrome to /static/index.html

Click "Choose Serial" and connect to an Urban Species Lightbox over USB. 

## Authors and acknowledgment
http://www.urbanspecies.org

Designed and coded by Thomas Laureyssens, Bart Derudder, Demetre Dzmanashvili and Stefaan Van Tuykom.

## License
CC BY-NC 3.0
